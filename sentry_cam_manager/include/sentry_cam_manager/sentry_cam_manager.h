/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/10/19.
//

#ifndef SENTRY_MX_SENTRY_CAM_MANAGER_H
#define SENTRY_MX_SENTRY_CAM_MANAGER_H

#include <ds_base/ds_process.h>
#include <ds_mx_msgs/StdPayloadCommand.h>

namespace sentry_mx {

class SentryCamManager : public ds_base::DsProcess {
  DS_DISABLE_COPY(SentryCamManager);

 public:
  explicit SentryCamManager();
  SentryCamManager(int argc, char* argv[], const std::string& name);
  ~SentryCamManager() override;

  void setupParameters() override;
  void setupSubscriptions() override;
  void setupConnections() override;
  void setupServices() override;
  void setupTimers() override;

 protected:
  ros::Subscriber cmd_sub_;
  ros::ServiceClient pwr_srv_;
  boost::shared_ptr<ds_asio::DsConnection> cam_conn_;

  // camera parameters
  double cam_gain;
  double cam_framerate;
  double cam_exposure; // ms?

  // driver overhead
  double poweroff_timeout;
  ros::Timer poweroff_timer;
  std::vector<uint16_t> power_addrs;

  void _cmd_callback(const ds_mx_msgs::StdPayloadCommand& cmd);
  void _from_camera_callback(const ds_core_msgs::RawData& bytes);
  void _shutdown_callback(const ros::TimerEvent& evt);
  void _start_shutdown_timer();
  void _cancel_shutdown_timer();

  std::string buildParamString(const ds_mx_msgs::StdPayloadCommand& cmd);
  void setPower(bool on);
};

} // namespace sentry_mx

#endif //SENTRY_MX_SENTRY_CAM_MANAGER_H
