/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/10/19.
//

#include <sentry_msgs/PWRCmd.h>
#include "sentry_cam_manager/sentry_cam_manager.h"
#include "sail_grd/pwr_lookup.h"

namespace sentry_mx {

SentryCamManager::SentryCamManager() : ds_base::DsProcess() {
  cam_gain = 10;
  cam_framerate = 0.3;
  cam_exposure = 20;
  poweroff_timeout = 300;
}

SentryCamManager::SentryCamManager(int argc, char* argv[], const std::string& name)
: ds_base::DsProcess(argc, argv, name) {
  cam_gain = 10;
  cam_framerate = 0.3;
  cam_exposure = 20;
  poweroff_timeout = 300;
}

SentryCamManager::~SentryCamManager() = default;

void SentryCamManager::setupParameters() {
  ros::NodeHandle nh = nodeHandle();

  nh.getParam(ros::this_node::getName() + "/default_gain", cam_gain);
  nh.getParam(ros::this_node::getName() + "/default_framerate", cam_framerate);
  nh.getParam(ros::this_node::getName() + "/default_exposure", cam_exposure);

  nh.getParam(ros::this_node::getName() + "/poweroff_timeout", poweroff_timeout);

  power_addrs.clear();
  std::string pwrname;

  // load power names
  pwrname = "camera";
  nh.getParam(ros::this_node::getName() + "/power_cam", pwrname);
  power_addrs.push_back(grd_util::get_address(pwrname));

  pwrname = "strobe";
  nh.getParam(ros::this_node::getName() + "/power_strobe", pwrname);
  power_addrs.push_back(grd_util::get_address(pwrname));

  pwrname = "laser";
  nh.getParam(ros::this_node::getName() + "/power_laser", pwrname);
  power_addrs.push_back(grd_util::get_address(pwrname));
}

void SentryCamManager::setupSubscriptions() {
  ds_base::DsProcess::setupSubscriptions();
  ros::NodeHandle nh = nodeHandle();

  cmd_sub_ = nh.subscribe("sentry_cam_cmd", 10,
      &SentryCamManager::_cmd_callback, this);
}

void SentryCamManager::setupServices() {
  ds_base::DsProcess::setupServices();
  ros::NodeHandle nh = nodeHandle();
  pwr_srv_ = nh.serviceClient<sentry_msgs::PWRCmd>("pwr_service");
}

void SentryCamManager::setupConnections() {
  ds_base::DsProcess::setupConnections();
  cam_conn_ = addConnection("camera", boost::bind(&SentryCamManager::_from_camera_callback, this, _1));
}

void SentryCamManager::setupTimers() {
  ds_base::DsProcess::setupTimers();
  ros::NodeHandle nh = nodeHandle();
  poweroff_timer = nh.createTimer(ros::Duration(poweroff_timeout),
      &SentryCamManager::_shutdown_callback, this, true, false);
}

void SentryCamManager::_cmd_callback(const ds_mx_msgs::StdPayloadCommand& cmd) {
  std::string msg = buildParamString(cmd);
  switch(cmd.command) {
    case ds_mx_msgs::StdPayloadCommand::COMMAND_START:
      ROS_INFO_STREAM("Starting camera...");
      ROS_INFO_STREAM("    powering on camera/strobe/lasers");
      _cancel_shutdown_timer();
      setPower(true);
      ROS_INFO_STREAM("    sending config \"" <<msg <<"\"");
      cam_conn_->send(msg);
      break;

    case ds_mx_msgs::StdPayloadCommand::COMMAND_STOP:
      ROS_INFO_STREAM("Stopping camera...");
      _start_shutdown_timer();
      break;

    case ds_mx_msgs::StdPayloadCommand::COMMAND_CONFIGONLY:
      ROS_INFO_STREAM("Updating camera config...");
      ROS_INFO_STREAM("    sending config \"" <<msg <<"\"");
      _cancel_shutdown_timer();
      cam_conn_->send(msg);
      break;

    default:
      ROS_ERROR_STREAM("Unsupported standard payload command " <<cmd.command);
  }
}

void SentryCamManager::_from_camera_callback(const ds_core_msgs::RawData& bytes) {
  std::string bytestr(bytes.data.begin(), bytes.data.end());
  ROS_INFO_STREAM("Got data from camera: " <<bytestr);
}

void SentryCamManager::_start_shutdown_timer() {
  poweroff_timer.stop();
  poweroff_timer.start();
}

void SentryCamManager::_cancel_shutdown_timer() {
  poweroff_timer.stop();
}

void SentryCamManager::_shutdown_callback(const ros::TimerEvent &evt) {
  setPower(false);
}

std::string SentryCamManager::buildParamString(const ds_mx_msgs::StdPayloadCommand& cmd) {
  std::stringstream ret;

  for (const auto& p : cmd.config) {
    if (p.key == "exposure") {
      float tmp=0;
      if (sscanf(p.value.c_str(), "%f", &tmp) == 1) {
        cam_exposure = tmp;
      } else {
        ROS_ERROR_STREAM("Unable to parse exposure value \"" <<p.value <<"\"");
      }
    }
    if (p.key == "framerate") {
      float tmp=0;
      if (sscanf(p.value.c_str(), "%f", &tmp) == 1) {
        cam_framerate = tmp;
      } else {
        ROS_ERROR_STREAM("Unable to parse framerate value \"" <<p.value <<"\"");
      }
    }
    if (p.key == "gain") {
      float tmp=0;
      if (sscanf(p.value.c_str(), "%f", &tmp) == 1) {
        cam_gain = tmp;
      } else {
        ROS_ERROR_STREAM("Unable to parse gain value \"" <<p.value <<"\"");
      }
    }
  }
  ret <<"CAM " <<cam_gain <<" " <<cam_framerate <<" " <<cam_exposure;

  return ret.str();
}

void SentryCamManager::setPower(bool on) {

  for (auto addr : this->power_addrs) {
    sentry_msgs::PWRCmd cmd;
    cmd.request.address = addr;
    if (on) {
      cmd.request.command = sentry_msgs::PWRCmd::Request::PWR_CMD_ON;
    } else {
      cmd.request.command = sentry_msgs::PWRCmd::Request::PWR_CMD_OFF;
    }

    if (!pwr_srv_.call(cmd)) {
      ROS_ERROR_STREAM("Unable to set power state of address: " <<addr);
    }
  }
}

} // namespace sentry_mx
