/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/30/19.
//

#ifndef SENTRY_MX_SENTRY_MANAGER_H
#define SENTRY_MX_SENTRY_MANAGER_H

#include <ds_param/ds_param.h>
#include <ds_param/ds_param_conn.h>
#include <ds_actuator_msgs/DropweightCmd.h>
#include <ds_control_msgs/GoalLegLatLon.h>
#include <ds_control_msgs/ControllerEnum.h>
#include <ds_control_msgs/JoystickEnum.h>
#include <sentry_msgs/SentryAllocationEnum.h>
#include <sentry_msgs/SentryControllerEnum.h>
#include <sentry_msgs/SentryJoystickEnum.h>
#include <sentry_msgs/InitiateBallastTest.h>
#include <sentry_msgs/XRCmd.h>
#include <sentry_msgs/SentryMxGoal.h>

#include <ds_base/ds_process.h>

namespace sentry_mx {

class SentryManager : public ds_base::DsProcess {
  DS_DISABLE_COPY(SentryManager);

 public:
  explicit SentryManager();
  SentryManager(int argc, char* argv[], const std::string& name);
  ~SentryManager() override;

  void setupParameters() override;
  void setupSubscriptions() override;
  void setupPublishers() override;
  void setupServices() override;

 protected:

  // /////////////////////////////////////////////////////////////////////// //
  // Outputs for setting core control modes
  ds_param::ParamConnection::Ptr conn_;
  ds_param::IntParam::Ptr active_controller_;
  ds_param::IntParam::Ptr active_allocation_;
  ds_param::IntParam::Ptr active_joystick_;

  ros::Publisher pub_joystick;
  ros::Publisher pub_leg_goal;
  ros::ServiceClient srv_ballast;
  ros::ServiceClient srv_xr1_cmd;
  ros::ServiceClient srv_xr2_cmd;

  // we have to fill in ALL the bottom follower parameters.  Yay.
  ds_param::DoubleParam::Ptr bf_altitude;
  ds_param::DoubleParam::Ptr bf_speed;
  ds_param::DoubleParam::Ptr bf_depth_floor;
  ds_param::DoubleParam::Ptr bf_envelope;
  ds_param::DoubleParam::Ptr bf_gain;
  ds_param::DoubleParam::Ptr bf_depthvel;
  ds_param::DoubleParam::Ptr bf_depthacc;
  ds_param::DoubleParam::Ptr bf_min_speed;
  ds_param::DoubleParam::Ptr bf_alarm_timeout;

  // /////////////////////////////////////////////////////////////////////// //
  // Input / handler for commands from MX vehicle primitives
  ros::Subscriber sub_cmd;

  void handleGoal(const sentry_msgs::SentryMxGoal& goal);

  void setIdleGoal(const sentry_msgs::SentryMxGoal& goal);
  void setJoystickGoal(const sentry_msgs::SentryMxGoal& goal);
  void setSurveyGoal(const sentry_msgs::SentryMxGoal& goal);
  void setBallastGoal(const sentry_msgs::SentryMxGoal& goal);

  // /////////////////////////////////////////////////////////////////////// //
  // Input / handler for commands from MX for dropping weights
  ros::Subscriber sub_weights;

  void handleWeights(const ds_actuator_msgs::DropweightCmd& cmd);

  // /////////////////////////////////////////////////////////////////////// //
  // Copy the multiple retries thing from the MC shim
  template <typename T>
  bool serviceRetry(ros::ServiceClient& srv, T& msg, size_t retries,
      const std::string& success_msg, const std::string& fail_msg) {

    for (size_t i=0; i<retries; i++) {
      if (srv.call(msg)) {
        ROS_INFO_STREAM(success_msg);
        return true;
      }
    }
    ROS_ERROR_STREAM(fail_msg);
    return false;
  }
};


} // namespace sentry_mx

#endif //SENTRY_MX_SENTRY_MANAGER_H
