/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/30/19.
//

#include "sentry_manager/sentry_manager.h"

#include <ds_param/ds_param_guard.h>
#include <sentry_msgs/InitiateBallastTest.h>

namespace sentry_mx {

SentryManager::SentryManager() : ds_base::DsProcess()
{
}

SentryManager::SentryManager(int argc, char* argv[], const std::string& name)
: ds_base::DsProcess(argc, argv, name)
{
}

SentryManager::~SentryManager() = default;

void SentryManager::setupParameters() {
  ds_base::DsProcess::setupParameters();
  ros::NodeHandle nh = nodeHandle();
  conn_ = ds_param::ParamConnection::create(nh);

  // connect to the active controller/allocation stuff
  std::string controller_ns = ros::param::param<std::string>("~controller_ns", "0");
  std::string goals_ns = ros::param::param<std::string>("~goals_ns", "0");
  active_controller_ = conn_->connect<ds_param::IntParam>(controller_ns + "/active_controller");
  active_allocation_ = conn_->connect<ds_param::IntParam>(controller_ns + "/active_allocation");
  active_joystick_ = conn_->connect<ds_param::IntParam>(goals_ns + "/active_joystick");

  // connect to the bottomfollower parameters
  std::string bf_ns = ros::param::param<std::string>("~bf_ns", "/sentry/bf");
  bf_altitude = conn_->connect<ds_param::DoubleParam>(bf_ns + "/altitude");
  bf_speed = conn_->connect<ds_param::DoubleParam>(bf_ns + "/speed");
  bf_depth_floor = conn_->connect<ds_param::DoubleParam>(bf_ns + "/depth_floor");

  bf_envelope = conn_->connect<ds_param::DoubleParam>(bf_ns + "/envelope");
  bf_gain = conn_->connect<ds_param::DoubleParam>(bf_ns + "/speed_gain");
  bf_depthvel = conn_->connect<ds_param::DoubleParam>(bf_ns + "/depth_rate");
  bf_depthacc = conn_->connect<ds_param::DoubleParam>(bf_ns + "/depth_acceleration");
  bf_min_speed = conn_->connect<ds_param::DoubleParam>(bf_ns + "/speed_minimum");
  bf_alarm_timeout = conn_->connect<ds_param::DoubleParam>(bf_ns + "/alarm_timeout");
}

void SentryManager::setupSubscriptions() {
  ds_base::DsProcess::setupSubscriptions();
  ros::NodeHandle nh = nodeHandle();

  sub_cmd = nh.subscribe("sentry_cmd", 10, &SentryManager::handleGoal, this);
  sub_weights = nh.subscribe("sentry_weight", 10, &SentryManager::handleWeights, this);
}

void SentryManager::setupPublishers() {
  ds_base::DsProcess::setupPublishers();
  ros::NodeHandle nh = nodeHandle();

  pub_joystick = nh.advertise<ds_nav_msgs::AggregatedState>("joystick", 10, false);
  pub_leg_goal = nh.advertise<ds_control_msgs::GoalLegLatLon>("leg_goal", 10, false);
}

void SentryManager::setupServices() {
  ds_base::DsProcess::setupServices();
  ros::NodeHandle nh = nodeHandle();

  srv_ballast = nh.serviceClient<sentry_msgs::InitiateBallastTest>("init_ballast_service");
  srv_xr1_cmd = nh.serviceClient<sentry_msgs::XRCmd>("xr1_cmd");
  srv_xr2_cmd = nh.serviceClient<sentry_msgs::XRCmd>("xr2_cmd");

  if (!srv_ballast.waitForExistence(ros::Duration(30))) {
    ROS_FATAL_STREAM("Timed out waiting for ballast check service!");
    ROS_BREAK();
  }

  if (!srv_xr1_cmd.waitForExistence(ros::Duration(30))) {
    ROS_FATAL_STREAM("Timed out waiting for XR1 command service!");
    ROS_BREAK();
  }

  if (!srv_xr2_cmd.waitForExistence(ros::Duration(30))) {
    ROS_FATAL_STREAM("Timed out waiting for XR2 command service!");
    ROS_BREAK();
  }
}

void SentryManager::handleGoal(const sentry_msgs::SentryMxGoal& goal) {

  // set parameters for each controller based on the selected controller
  switch (goal.controller_mode) {
    case sentry_msgs::SentryControllerEnum::NONE:
      setIdleGoal(goal);
      break;

    case sentry_msgs::SentryControllerEnum::JOYSTICK:
      setJoystickGoal(goal);
      break;

    case sentry_msgs::SentryControllerEnum::SURVEY:
      setSurveyGoal(goal);
      break;

    case sentry_msgs::SentryControllerEnum::BALLAST:
      setBallastGoal(goal);
      break;

    default:
      ROS_ERROR_STREAM("Unknown controller type " <<goal.controller_mode <<", ignoring!");
      // do nothing
  }
}

void SentryManager::setIdleGoal(const sentry_msgs::SentryMxGoal& goal) {
  active_controller_->Set(goal.controller_mode);
  active_allocation_->Set(goal.allocator_mode);
  active_joystick_->Set(goal.joystick_mode);
  ROS_INFO_STREAM("Setting IDLE goal!");
}

void SentryManager::setJoystickGoal(const sentry_msgs::SentryMxGoal& goal) {
  active_controller_->Set(goal.controller_mode);
  active_allocation_->Set(goal.allocator_mode);
  active_joystick_->Set(goal.joystick_mode);

  ROS_INFO_STREAM("Setting Joystick goal!");
  if (goal.joystick_mode == ds_control_msgs::JoystickEnum::MC) {
    // we have to pass the commanded joystick value on
    ROS_INFO_STREAM("Setting joystick goal...");
    pub_joystick.publish(goal.commanded_state);
  }
}

void SentryManager::setSurveyGoal(const sentry_msgs::SentryMxGoal& goal) {
  active_controller_->Set(goal.controller_mode);
  active_allocation_->Set(sentry_msgs::SentryAllocationEnum::FLIGHT_MODE);
  active_joystick_->Set(goal.joystick_mode);

  // send the trackline
  ds_control_msgs::GoalLegLatLon leg;
  leg.header.stamp = goal.stamp;

  leg.line_start = goal.trackline_start;
  leg.line_end = goal.trackline_end;

  ROS_INFO_STREAM("Setting trackline goal to (" <<leg.line_start.longitude <<", "
                                                <<leg.line_start.latitude <<") --> ("
                                                <<leg.line_end.longitude <<", "
                                                <<leg.line_end.latitude <<")");

  pub_leg_goal.publish(leg);

  // send the bottom-follower goals
  {
    ROS_INFO_STREAM("Setting survey controller goals...");
    ds_param::ParamGuard guard(conn_); // prevent ds_param from sending until this object passes
                                 // out of scope
    if (goal.altitude.valid == ds_nav_msgs::FlaggedDouble::VALUE_VALID) {
      ROS_INFO_STREAM("\tSetting altitude goal to " <<goal.altitude.value);
      bf_altitude->Set(goal.altitude.value);
    }
    if (std::isfinite(goal.bf_envelope)) {
      ROS_INFO_STREAM("\tSetting bf param envelope to " <<goal.bf_envelope);
      bf_envelope->Set(goal.bf_envelope);
    }
    if (goal.commanded_state.surge_u.valid == ds_nav_msgs::FlaggedDouble::VALUE_VALID) {
      ROS_INFO_STREAM("\tSetting bf_speed to " <<goal.commanded_state.surge_u.value);
      bf_speed->Set(goal.commanded_state.surge_u.value);
    }
    if (std::isfinite(goal.bf_gain)) {
      ROS_INFO_STREAM("\tSetting bf gain param to " <<goal.bf_gain);
      bf_gain->Set(goal.bf_gain);
    }
    if (std::isfinite(goal.bf_depthvel)) {
      ROS_INFO_STREAM("\tSetting bf param depthvel to " <<goal.bf_depthvel);
      bf_depthvel->Set(goal.bf_depthvel);
    }
    if (std::isfinite(goal.bf_depthacc)) {
      ROS_INFO_STREAM("\tSetting bf param depthacc to " <<goal.bf_depthacc);
      bf_depthacc->Set(goal.bf_depthacc);
    }
    if (std::isfinite(goal.bf_min_speed)) {
      ROS_INFO_STREAM("\tSetting bf param min_speed to " <<goal.bf_min_speed);
      bf_min_speed->Set(goal.bf_min_speed);
    }
    if (std::isfinite(goal.bf_alarm_timeout)) {
      ROS_INFO_STREAM("\tSetting bf param alarm_timeout to " <<goal.bf_alarm_timeout);
      bf_alarm_timeout->Set(goal.bf_alarm_timeout);
    }
    if (goal.depth_floor.valid == ds_nav_msgs::FlaggedDouble::VALUE_VALID) {
      ROS_INFO_STREAM("\tSetting depth floor to " <<goal.depth_floor.value);
      bf_depth_floor->Set(goal.depth_floor.value);
    }

  }
}

void SentryManager::setBallastGoal(const sentry_msgs::SentryMxGoal& goal) {
  active_controller_->Set(goal.controller_mode);
  active_allocation_->Set(goal.allocator_mode);
  active_joystick_->Set(goal.joystick_mode);

  sentry_msgs::InitiateBallastTest scmd;
  if (goal.altitude.valid == ds_nav_msgs::FlaggedDouble::VALUE_VALID
    && goal.depth_floor.valid == ds_nav_msgs::FlaggedDouble::VALUE_VALID) {
    // we're commanded into auto-altitude mode

    // first, set the depth floor in the bottom follower correctly...
    bf_depth_floor->Set(goal.depth_floor.value);

    ROS_INFO_STREAM("sentry_manager requesting hold altitude " << goal.altitude.value);

    // next, set the service command
    scmd.request.hold_altitude = true;
    scmd.request.ballast_testpoint = goal.altitude.value;
  } else if (goal.commanded_state.down.valid == ds_nav_msgs::FlaggedDouble::VALUE_VALID) {
    // go to auto-depth mode
    scmd.request.hold_altitude = false;
    scmd.request.ballast_testpoint = goal.commanded_state.down.value;
  } else {
    // got an invalid goal request; hold current altitude by setting hold altitude to
    // false and ballast_testpoing to 0
    scmd.request.hold_altitude = false;
    scmd.request.ballast_testpoint = -1;
  }

  // actually send the service command; try several times
  ROS_INFO_STREAM("MX setting ballast control, alt=" <<scmd.request.hold_altitude
                                                 << " setpoint=" <<scmd.request.ballast_testpoint);

  serviceRetry(srv_ballast, scmd, 10, "Ballast controller initialized!",
      "Ballast controller setup FAILED");
}

void SentryManager::handleWeights(const ds_actuator_msgs::DropweightCmd& cmd) {
  sentry_msgs::XRCmd xr1_cmd;
  sentry_msgs::XRCmd xr2_cmd;

  // ok, the way the d-cam/burnwires commands work is kinda unfortunate.  They're not orthogonal.
  // Still, we'll make it work.

  // XR1, dcam
  if (cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]) {
    // port
    xr1_cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_DCAM_OPEN_2;
    serviceRetry(srv_xr1_cmd, xr1_cmd, 10, "XR1 command sent!", "XR1 command FAILED!");
    ROS_INFO_STREAM("Dropping Port Ascent");
  }

  // XR2, dcam
  if (cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]
      && cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]) {
    // descent + stbd
    xr2_cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_DCAM_OPEN;
    serviceRetry(srv_xr2_cmd, xr2_cmd, 10, "XR2 command sent!", "XR2 command FAILED!");
    ROS_INFO_STREAM("Dropping Descent");
    ROS_INFO_STREAM("Dropping Starboard Ascent");

  } else if (cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]) {
    // descent
    xr2_cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_DCAM_OPEN_1;
    serviceRetry(srv_xr2_cmd, xr2_cmd, 10, "XR2 command sent!", "XR2 command FAILED!");
    ROS_INFO_STREAM("Dropping Descent");

  } else if (cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]) {
    //stbd
    xr2_cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_DCAM_OPEN_2;
    serviceRetry(srv_xr2_cmd, xr2_cmd, 10, "XR2 command sent!", "XR2 command FAILED!");
    ROS_INFO_STREAM("Dropping Starboard Ascent");
  }

  // XR1, burnwire
  if (cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]
      && cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]) {
    // descent + stbd
    xr1_cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_ON;
    serviceRetry(srv_xr1_cmd, xr1_cmd, 10, "XR1 command sent!", "XR1 command FAILED!");

  } else if (cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]) {
    // descent
    xr1_cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_ON_1;
    serviceRetry(srv_xr1_cmd, xr1_cmd, 10, "XR1 command sent!", "XR1 command FAILED!");

  } else if (cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]) {
    // stbd
    xr1_cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_ON_2;
    serviceRetry(srv_xr1_cmd, xr1_cmd, 10, "XR1 command sent!", "XR1 command FAILED!");
  }

  // XR2, burnwire
  if (cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]) {
    // port
    xr2_cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_ON_2;
    serviceRetry(srv_xr2_cmd, xr2_cmd, 10, "XR2 command sent!", "XR2 command FAILED!");
  }
}

} // namespace sentry_mx
