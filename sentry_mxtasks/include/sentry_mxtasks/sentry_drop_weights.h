/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//

#ifndef MX_SENTRY_DROP_WEIGHTS_H
#define MX_SENTRY_DROP_WEIGHTS_H

#include <ds_mxcore/ds_mxcore.h>
#include <ds_actuator_msgs/DropweightCmd.h>

namespace sentry_mxtasks {

class PayloadDropWeights : public ds_mx::MxPayloadTask {
 public:
  PayloadDropWeights();
  void init_ros(ros::NodeHandle &nh) override;
  bool validate() const override;

  void onStart(const ds_nav_msgs::NavState &state) override;
  void onStop(const ds_nav_msgs::NavState &state) override;

  ds_actuator_msgs::DropweightCmd buildCmdMessage(const ds_nav_msgs::NavState& state) const;

 protected:
  ds_mx::BoolParam drop_descent;
  ds_mx::BoolParam burn_descent;
  ds_mx::BoolParam drop_ascent;
  ds_mx::BoolParam burn_ascent;
  ds_mx::DoubleParam delay_before_seconds;
  ds_mx::DoubleParam delay_after_seconds;
  ds_mx::DoubleParam repeat_seconds;
  ds_mx::BoolParam success_when_done;

  ros::Time drop_time;
  ros::Time done_time;

  ros::Publisher cmd_pub_;
  bool dropped;

  ds_mx::TaskReturnCode onTick(const ds_nav_msgs::NavState& state);
  ds_mx::TaskReturnCode subtaskDone(const ds_nav_msgs::NavState& state,
                                    ds_mx::TaskReturnCode subtaskCode) override;
  void _drop(const ds_nav_msgs::NavState& state, const std::string& reason);
};

} // namespace sentry_mxtasks

#endif //MX_SENTRY_DROP_WEIGHTS_H
