/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/18/19.
//

#include <gtest/gtest.h>

#include <ds_mxutils/ds_test_message_queue.h>
#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <sentry_mxtasks/sentry_drop_weights.h>

namespace sentry_mxtasks {
namespace test {

class SentryDropweightRostest : public ::testing::Test {
 protected:

  static const char* JSON_STR;

  SentryDropweightRostest() : drop_msgs("sentry_dropweight_cmd"){
    node_handle.reset(new ros::NodeHandle());
  }

  void SetUp() override {
    // prepare our subtask
    subtask = std::make_shared<ds_mx::MxMockTask>();

    // prepare our compiler
    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    compiler->addSubtask("subtask", subtask);

    // read the config
    Json::Value config;
    Json::Reader reader;
    reader.parse(JSON_STR, config);

    // setup our test
    underTest = std::make_shared<sentry_mxtasks::PayloadDropWeights>();
    underTest->init(config, compiler);
    underTest->init_ros(*node_handle); // run the ROS setup, since this is the
                                       // rostest edition!

    // fill in our test state with some basic values
    state.header.stamp = ros::Time::now();
    state.lat = 1;
    state.lon = 2;
    state.down = 1000;
    state.heading = 0.5;
    state.roll = 0.01;
    state.pitch = 0.02;
  }

  std::shared_ptr<ds_mx::MxMockTask> subtask;
  std::shared_ptr<sentry_mxtasks::PayloadDropWeights> underTest;
  ds_nav_msgs::NavState state;

 protected:
  ds_mx::TestMessageQueue<ds_actuator_msgs::DropweightCmd> drop_msgs;

 private:
  std::unique_ptr<ros::NodeHandle> node_handle;
};

const char* SentryDropweightRostest::JSON_STR = R"( {
  "type": "sentry_drop_weights",
  "subtask": "subtask",
  "drop_descent": true,
  "burn_descent": false,
  "drop_ascent": false,
  "burn_ascent": false,
  "delay_before_seconds": 0,
  "delay_after_seconds": 0,
  "success_when_done": true
})";

TEST_F(SentryDropweightRostest, TestDropDescent) {

  // start the task
  underTest->onStart(state);

  // get the next message
  ds_actuator_msgs::DropweightCmd cmd;
  DSMX_EXPECT_MESSAGE(cmd, drop_msgs, 0.150);
  EXPECT_EQ(0, drop_msgs.numMessages()); // there should be only one

  EXPECT_EQ(state.header.stamp, cmd.stamp);
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

  EXPECT_TRUE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);

  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, rc);

  // expect no additional messages
  underTest->onStop(state);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

TEST_F(SentryDropweightRostest, TestDropAscent) {
  underTest->getParameters().get<ds_mx::BoolParam>("drop_descent").setFromString("false");
  underTest->getParameters().get<ds_mx::BoolParam>("drop_ascent").setFromString("true");

  // start the task
  underTest->onStart(state);

  // get the next message
  ds_actuator_msgs::DropweightCmd cmd;
  DSMX_EXPECT_MESSAGE(cmd, drop_msgs, 0.150);
  EXPECT_EQ(0, drop_msgs.numMessages()); // there should be only one

  EXPECT_EQ(state.header.stamp, cmd.stamp);
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_TRUE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);

  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, rc);

  // expect no additional messages
  underTest->onStop(state);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

TEST_F(SentryDropweightRostest, TestBurnDescent) {
  underTest->getParameters().get<ds_mx::BoolParam>("drop_descent").setFromString("false");
  underTest->getParameters().get<ds_mx::BoolParam>("burn_descent").setFromString("true");

  // start the task
  underTest->onStart(state);

  // get the next message
  ds_actuator_msgs::DropweightCmd cmd;
  DSMX_EXPECT_MESSAGE(cmd, drop_msgs, 0.150);
  EXPECT_EQ(0, drop_msgs.numMessages()); // there should be only one

  EXPECT_EQ(state.header.stamp, cmd.stamp);
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_TRUE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);

  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, rc);

  // expect no additional messages
  underTest->onStop(state);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

TEST_F(SentryDropweightRostest, TestBurnAscent) {
  underTest->getParameters().get<ds_mx::BoolParam>("drop_descent").setFromString("false");
  underTest->getParameters().get<ds_mx::BoolParam>("burn_ascent").setFromString("true");

  // start the task
  underTest->onStart(state);

  // get the next message
  ds_actuator_msgs::DropweightCmd cmd;
  DSMX_EXPECT_MESSAGE(cmd, drop_msgs, 0.150);
  EXPECT_EQ(0, drop_msgs.numMessages()); // there should be only one

  EXPECT_EQ(state.header.stamp, cmd.stamp);
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
  EXPECT_TRUE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);

  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, rc);

  // expect no additional messages
  underTest->onStop(state);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}
TEST_F(SentryDropweightRostest, TestKeepRunning) {

  underTest->getParameters().get<ds_mx::BoolParam>("success_when_done").setFromString("false");

  // start the task
  underTest->onStart(state);

  // get the next message
  ds_actuator_msgs::DropweightCmd cmd;
  DSMX_EXPECT_MESSAGE(cmd, drop_msgs, 0.150);
  EXPECT_EQ(0, drop_msgs.numMessages()); // there should be only one

  EXPECT_EQ(state.header.stamp, cmd.stamp);
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

  EXPECT_TRUE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);

  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // expect no additional messages
  underTest->onStop(state);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

TEST_F(SentryDropweightRostest, TestDelayBefore) {

  underTest->getParameters().get<ds_mx::DoubleParam>("delay_before_seconds").setFromString("1.0");

  // start the task
  underTest->onStart(state);

  // first tick should not drop weights
  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  // however, at/after the specified time...
  state.header.stamp += ros::Duration(1.00);

  rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // get the next message
  ds_actuator_msgs::DropweightCmd cmd;
  DSMX_EXPECT_MESSAGE(cmd, drop_msgs, 0.150);
  EXPECT_EQ(0, drop_msgs.numMessages()); // there should be only one

  EXPECT_EQ(state.header.stamp, cmd.stamp);
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

  EXPECT_TRUE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);

  // expect no additional messages
  underTest->onStop(state);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

TEST_F(SentryDropweightRostest, TestDelayBeforeCancelled) {

  underTest->getParameters().get<ds_mx::DoubleParam>("delay_before_seconds").setFromString("10.0");

  // start the task
  underTest->onStart(state);

  // first tick should not drop weights
  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  // We step too short a time period
  state.header.stamp += ros::Duration(1.00);

  rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  // now, let's say the task gets stopped...
  underTest->onStop(state);

  // This should produce a message.  Look for it...
  ds_actuator_msgs::DropweightCmd cmd;
  DSMX_EXPECT_MESSAGE(cmd, drop_msgs, 0.150);
  EXPECT_EQ(0, drop_msgs.numMessages()); // there should be only one

  EXPECT_EQ(state.header.stamp, cmd.stamp);
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

  EXPECT_TRUE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);

  // expect no additional messages
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

TEST_F(SentryDropweightRostest, TestDelayAfter) {

  underTest->getParameters().get<ds_mx::DoubleParam>("delay_after_seconds").setFromString("1.0");

  // start the task
  underTest->onStart(state);

  // first tick should drop weights
  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // get the next message
  ds_actuator_msgs::DropweightCmd cmd;
  DSMX_EXPECT_MESSAGE(cmd, drop_msgs, 0.150);
  EXPECT_EQ(0, drop_msgs.numMessages()); // there should be only one

  EXPECT_EQ(state.header.stamp, cmd.stamp);
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

  EXPECT_TRUE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);

  state.header.stamp += ros::Duration(1.00);
  rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, rc);

  // expect no additional messages
  underTest->onStop(state);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

TEST_F(SentryDropweightRostest, TestDelayBeforeAndAfter) {

  underTest->getParameters().get<ds_mx::DoubleParam>("delay_before_seconds").setFromString("1.0");
  underTest->getParameters().get<ds_mx::DoubleParam>("delay_after_seconds").setFromString("1.0");

  // start the task
  underTest->onStart(state);

  // first tick should not drop weights
  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  // however, at/after the specified time...
  state.header.stamp += ros::Duration(1.00);
  rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // get the next message
  ds_actuator_msgs::DropweightCmd cmd;
  DSMX_EXPECT_MESSAGE(cmd, drop_msgs, 0.150);
  EXPECT_EQ(0, drop_msgs.numMessages()); // there should be only one

  EXPECT_EQ(state.header.stamp, cmd.stamp);
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

  EXPECT_TRUE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);

  state.header.stamp += ros::Duration(1.00);
  rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, rc);

  // expect no additional messages
  underTest->onStop(state);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

TEST_F(SentryDropweightRostest, TestDelayBeforeAndAfterNoReturn) {

  underTest->getParameters().get<ds_mx::DoubleParam>("delay_before_seconds").setFromString("1.0");
  underTest->getParameters().get<ds_mx::DoubleParam>("delay_after_seconds").setFromString("1.0");
  underTest->getParameters().get<ds_mx::BoolParam>("success_when_done").setFromString("false");

  // start the task
  underTest->onStart(state);

  // first tick should not drop weights
  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  // however, at/after the specified time...
  state.header.stamp += ros::Duration(1.00);
  rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // get the next message
  ds_actuator_msgs::DropweightCmd cmd;
  DSMX_EXPECT_MESSAGE(cmd, drop_msgs, 0.150);
  EXPECT_EQ(0, drop_msgs.numMessages()); // there should be only one

  EXPECT_EQ(state.header.stamp, cmd.stamp);
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

  EXPECT_TRUE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);

  state.header.stamp += ros::Duration(1.00);
  rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // expect no additional messages
  underTest->onStop(state);
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

TEST_F(SentryDropweightRostest, TestRepeatDrop) {

  underTest->getParameters().get<ds_mx::DoubleParam>("delay_before_seconds").setFromString("0.0");
  underTest->getParameters().get<ds_mx::DoubleParam>("delay_after_seconds").setFromString("0.0");
  underTest->getParameters().get<ds_mx::DoubleParam>("repeat_seconds").setFromString("10.0");
  underTest->getParameters().get<ds_mx::BoolParam>("success_when_done").setFromString("false");

  // start the task
  underTest->onStart(state);

  for (size_t i=0; i<10; i++) {
    ds_mx::TaskReturnCode rc = underTest->tick(state);
    EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);
    if (i % 2 == 0) {
      // even ticks should have weight drop messages.  Odd ticks won't.
      ds_actuator_msgs::DropweightCmd cmd;
      DSMX_EXPECT_MESSAGE(cmd, drop_msgs, 0.150);
      EXPECT_EQ(0, drop_msgs.numMessages()); // there should be only one

      EXPECT_EQ(state.header.stamp, cmd.stamp);
      ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
      ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

      EXPECT_TRUE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
      EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
      EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
      EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
    } else {
      // no message expected
      EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
    }
    state.header.stamp += ros::Duration(5.00);
  }

  // Because we're abusing the dropped flag slightly, onStop will still trigger an outgoing drop command
  // However, that seems pretty reasonable for repeated drop behavior.  So we'll simply decide to expect it!
  underTest->onStop(state);
  ds_actuator_msgs::DropweightCmd cmd;
  DSMX_EXPECT_MESSAGE(cmd, drop_msgs, 0.150);
  EXPECT_EQ(0, drop_msgs.numMessages()); // there should be only one
  EXPECT_FALSE(drop_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

} // namespace test
} // namespace sentry_mxtasks


int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "rostest_sentry_drop_weights");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(2); // use two threads
  spinner.start(); // returns immediately

  ds_mx::eventlog_disable();

  return RUN_ALL_TESTS();
}
