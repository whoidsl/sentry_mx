/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 8/20/19.
//

#include <gtest/gtest.h>

#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <sentry_mxtasks/sentry_drop_weights.h>

namespace sentry_mxtasks {
namespace test {

class SentryDropweightTest : public ::testing::Test {
 protected:

  static const char* JSON_STR;

  void SetUp() override {
    // prepare our subtask
    subtask = std::make_shared<ds_mx::MxMockTask>();

    // prepare our compiler
    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    compiler->addSubtask("subtask", subtask);

    // read the config
    Json::Value config;
    Json::Reader reader;
    reader.parse(JSON_STR, config);

    // setup our test
    underTest = std::make_shared<sentry_mxtasks::PayloadDropWeights>();
    underTest->init(config, compiler);

    // fill in our test state with some basic values
    state.header.stamp = ros::Time::now();
    state.lat = 1;
    state.lon = 2;
    state.down = 1000;
    state.heading = 0.5;
    state.roll = 0.01;
    state.pitch = 0.02;
  }

  std::shared_ptr<ds_mx::MxMockTask> subtask;
  std::shared_ptr<sentry_mxtasks::PayloadDropWeights> underTest;
  ds_nav_msgs::NavState state;
};

const char* SentryDropweightTest::JSON_STR = R"( {
  "type": "sentry_drop_weights",
  "subtask": "subtask",
  "drop_descent": true,
  "burn_descent": false,
  "drop_ascent": false,
  "burn_ascent": false,
  "delay_before_seconds": 0,
  "delay_after_seconds": 0,
  "success_when_done": true
})";

TEST_F(SentryDropweightTest, TestValidateNoDrops) {
  // initial configuration is valid
  EXPECT_TRUE(underTest->validate());

  // but not having a timeout is invalid
  underTest->getParameters().get<ds_mx::BoolParam>("drop_descent").setFromString("false");
  underTest->getParameters().get<ds_mx::BoolParam>("burn_descent").setFromString("false");
  underTest->getParameters().get<ds_mx::BoolParam>("drop_ascent").setFromString("false");
  underTest->getParameters().get<ds_mx::BoolParam>("burn_ascent").setFromString("false");
  EXPECT_FALSE(underTest->validate());
}

TEST_F(SentryDropweightTest, TestValidateNegativeDelayBefore) {
  // initial configuration is valid
  EXPECT_TRUE(underTest->validate());

  underTest->getParameters().get<ds_mx::DoubleParam>("delay_before_seconds").setFromString("-1");
  EXPECT_FALSE(underTest->validate());
}

TEST_F(SentryDropweightTest, TestValidateNegativeDelayAfter) {
  // initial configuration is valid
  EXPECT_TRUE(underTest->validate());

  underTest->getParameters().get<ds_mx::DoubleParam>("delay_after_seconds").setFromString("-1");
  EXPECT_FALSE(underTest->validate());
}

TEST_F(SentryDropweightTest, TestValidateBadRepeatConfig) {
  // initial configuration is valid
  EXPECT_TRUE(underTest->validate());

  underTest->getParameters().get<ds_mx::BoolParam>("success_when_done").setFromString("true");
  underTest->getParameters().get<ds_mx::DoubleParam>("repeat_seconds").setFromString("1.0");
  EXPECT_FALSE(underTest->validate());
}

TEST_F(SentryDropweightTest, TestDisplay) {

  // setup the subtask's display
  ds_mx_msgs::MissionElementDisplay displayElements;
  displayElements.role = ds_mx_msgs::MissionElementDisplay::ROLE_IDLE;
  displayElements.label = "task label";
  displayElements.wellknowntext = "WKT_subtask";
  subtask->displayElement = displayElements;

  // actually get the display
  ds_mx_msgs::MissionDisplay result;
  underTest->getDisplay(state, result);

  boost::uuids::uuid uuid_res;
  EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_IDLE, result.elements[0].role);
  std::copy(result.elements[0].task_uuid.begin(), result.elements[0].task_uuid.end(), uuid_res.begin());
  EXPECT_EQ(subtask->getUuid(), uuid_res);
  EXPECT_EQ("WKT_subtask", result.elements[0].wellknowntext);
  EXPECT_EQ("task label", result.elements[0].label);
}

TEST_F(SentryDropweightTest, TestBuildCmd) {
  ds_actuator_msgs::DropweightCmd cmd = underTest->buildCmdMessage(state);

  EXPECT_EQ(state.header.stamp, cmd.stamp);
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

  EXPECT_TRUE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
}

TEST_F(SentryDropweightTest, TestBuildBurnCmd) {

  underTest->getParameters().get<ds_mx::BoolParam>("drop_descent").setFromString("false");
  underTest->getParameters().get<ds_mx::BoolParam>("burn_descent").setFromString("true");

  ds_actuator_msgs::DropweightCmd cmd = underTest->buildCmdMessage(state);

  EXPECT_EQ(state.header.stamp, cmd.stamp);
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_DESCENT, cmd.drop.size());
  ASSERT_LE(ds_actuator_msgs::DropweightCmd::IDX_ASCENT, cmd.drop.size());

  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_TRUE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT]);
  EXPECT_FALSE(cmd.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
  EXPECT_FALSE(cmd.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT]);
}

} // namespace test
} // namespace sentry_mxtasks