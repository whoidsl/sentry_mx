/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 8/20/19.
//


#include <sentry_mxtasks/sentry_trackbf.h>
#include "test_sentry_common.h"

#include <sentry_msgs/SentryJoystickEnum.h>
#include <sentry_msgs/SentryControllerEnum.h>
#include <sentry_msgs/SentryAllocationEnum.h>

namespace sentry_mxtasks {
namespace test {

// These primitives have very same-y google test classes.  Let's use a common base class
// and override the JSON description string.
class SentryTrackBfTest : public SentryMxPrimitiveTest<TaskTracklineBf> {
 protected:
  virtual std::string getJSON() const {
    return R"( {
  "type": "sentry_trackbf",
  "start_pt": "POINT(2.0001 1.0001) ",
  "end_pt": "POINT(2.0001 1.0002) ",
  "speed": 1.0,
  "altitude": 65,
  "depth_floor": 6000,
  "envelope": 20.0,
  "gain": 4.0,
  "depthvel": 0.33,
  "depthacc": 0.07,
  "min_speed": 0.4,
  "alarm_timeout": 15.0,
  "timeout_multiplier": 1.5,
  "timeout_increment": 45
})";
  }
};

TEST_F(SentryTrackBfTest, TestValidate) {
  // initial configuration is valid
  EXPECT_TRUE(underTest->validate());

  // speed must be >= 0
  underTest->getParameters().get<ds_mx::DoubleParam>("speed").set(-0.000001);
  EXPECT_FALSE(underTest->validate());
}

TEST_F(SentryTrackBfTest, TestDisplay) {
  ds_mx_msgs::MissionDisplay display;

  underTest->getDisplay(state, display);

  ASSERT_EQ(2, display.elements.size());

  EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING, display.elements[0].role);
  EXPECT_EQ("LINESTRING (2.000000000 1.000000000, 2.000100000 1.000100000)", display.elements[0].wellknowntext);

  EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE, display.elements[1].role);
  EXPECT_EQ("LINESTRING (2.000100000 1.000100000, 2.000100000 1.000200000)", display.elements[1].wellknowntext);
}

TEST_F(SentryTrackBfTest, TestBuildCmd) {
  sentry_msgs::SentryMxGoal goal = underTest->buildCmdMessage(state);

  // check controller modes
  EXPECT_EQ(sentry_msgs::SentryControllerEnum::SURVEY, goal.controller_mode);
  EXPECT_EQ(sentry_msgs::SentryAllocationEnum::FLIGHT_MODE, goal.allocator_mode);

  EXPECT_EQ(state.header.stamp, goal.stamp);

  // basic bottom-follower stuff
  EXPECT_TRUE(goal.altitude.valid);
  EXPECT_DOUBLE_EQ(65, goal.altitude.value);

  EXPECT_TRUE(goal.commanded_state.surge_u.valid);
  EXPECT_DOUBLE_EQ(1.0, goal.commanded_state.surge_u.value);

  EXPECT_TRUE(goal.depth_floor.valid);
  EXPECT_DOUBLE_EQ(6000.0, goal.depth_floor.value);

  // fields to be ignored
  EXPECT_FALSE(goal.commanded_state.easting.valid);
  EXPECT_FALSE(goal.commanded_state.northing.valid);
  EXPECT_FALSE(goal.commanded_state.down.valid);
  EXPECT_FALSE(goal.commanded_state.roll.valid);
  EXPECT_FALSE(goal.commanded_state.pitch.valid);
  EXPECT_FALSE(goal.commanded_state.heading.valid);
  EXPECT_FALSE(goal.commanded_state.sway_v.valid);
  EXPECT_FALSE(goal.commanded_state.heave_w.valid);
  EXPECT_FALSE(goal.commanded_state.p.valid);
  EXPECT_FALSE(goal.commanded_state.q.valid);
  EXPECT_FALSE(goal.commanded_state.r.valid);

  // Now for the actual trackline
  EXPECT_DOUBLE_EQ(2.0001, goal.trackline_start.longitude);
  EXPECT_DOUBLE_EQ(1.0001, goal.trackline_start.latitude);

  EXPECT_DOUBLE_EQ(2.0001, goal.trackline_end.longitude);
  EXPECT_DOUBLE_EQ(1.0002, goal.trackline_end.latitude);

  EXPECT_FLOAT_EQ(20.0, goal.bf_envelope);
  EXPECT_FLOAT_EQ(4.0, goal.bf_gain);
  EXPECT_FLOAT_EQ(0.33, goal.bf_depthvel);
  EXPECT_FLOAT_EQ(0.07, goal.bf_depthacc);
  EXPECT_FLOAT_EQ(0.4, goal.bf_min_speed);
  EXPECT_FLOAT_EQ(15.0, goal.bf_alarm_timeout);
}

} // namespace test
} // namespace sentry_mxtasks