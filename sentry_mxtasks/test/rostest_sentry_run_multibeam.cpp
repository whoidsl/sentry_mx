/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/10/19.
//

#include <gtest/gtest.h>

#include <ds_mxutils/ds_test_message_queue.h>
#include <ds_mxcore/test/ds_mock_task.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <sentry_mxtasks/sentry_run_mb.h>

namespace sentry_mxtasks {
namespace test {

class SentryRunMultibeamRostest : public ::testing::Test {
 protected:

  static const char *JSON_STR;

  SentryRunMultibeamRostest() : cmd_msgs("sentry_mb_cmd") {
    node_handle.reset(new ros::NodeHandle());
  }

  void SetUp() override {
    // prepare our subtask
    subtask = std::make_shared<ds_mx::MxMockTask>();

    // prepare our compiler
    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    compiler->addSubtask("subtask", subtask);


    // setup our subtask's parameters
    example_param.reset(new ds_mx::GeoPointParam(
        subtask->getParametersPtr(), "example_param", ds_mx::DYNAMIC));

    // read the config
    Json::Value config;
    Json::Reader reader;
    reader.parse(JSON_STR, config);

    // setup our test
    underTest = std::make_shared<sentry_mxtasks::PayloadRunMultibeam>();
    underTest->init(config, compiler);
    underTest->init_ros(*node_handle); // run the ROS setup, since this is the
    // rostest edition!

    // fill in our test state with some basic values
    state.header.stamp = ros::Time::now();
    state.lat = 1;
    state.lon = 2;
    state.down = 1000;
    state.heading = 0.5;
    state.roll = 0.01;
    state.pitch = 0.02;
  }

  std::shared_ptr<ds_mx::MxMockTask> subtask;
  std::shared_ptr<sentry_mxtasks::PayloadRunMultibeam> underTest;
  ds_nav_msgs::NavState state;
  std::shared_ptr<ds_mx::GeoPointParam> example_param;

 protected:
  ds_mx::TestMessageQueue<ds_mx_msgs::StdPayloadCommand> cmd_msgs;

 private:
  std::unique_ptr<ros::NodeHandle> node_handle;
};

// payload parameters totally made up, but will show up
// alphabetically in the list
const char *SentryRunMultibeamRostest::JSON_STR = R"( {
  "type": "sentry_run_mb",
  "subtask": "subtask",
  "payload_params": {
    "ping_rate": 1,
    "detect_mode": "mixed",
    "force_depth": false
  }
})";

TEST_F(SentryRunMultibeamRostest, validateGood) {
  subtask->validateReturn = true;
  EXPECT_TRUE(underTest->validate());
  EXPECT_EQ(1, subtask->validateCalls);
}

TEST_F(SentryRunMultibeamRostest, validateBad) {
  subtask->validateReturn = false;
  EXPECT_FALSE(underTest->validate());
  EXPECT_EQ(1, subtask->validateCalls);
}

TEST_F(SentryRunMultibeamRostest, hasParam) {
  EXPECT_TRUE(underTest->getParameters().has<ds_mx::GeoPointParam>("example_param"));
  EXPECT_FALSE(underTest->getParameters().has<ds_mx::GeoPointParam>("doesnotexist"));
  EXPECT_FALSE(underTest->getParameters().has<ds_mx::IntParam>("example_param"));
}

// This is a super-standard task, so actual 'does this work' stuff is tested in the base class.
// Still, its good to check if the ROS stuff works, so let's do one I guess.

TEST_F(SentryRunMultibeamRostest, basic) {
  // start the task
  underTest->onStart(state);

  // get the next message
  ds_mx_msgs::StdPayloadCommand cmd;
  DSMX_EXPECT_MESSAGE(cmd, cmd_msgs, 0.150);
  EXPECT_EQ(0, cmd_msgs.numMessages()); // there should be only one

  // check
  EXPECT_EQ(state.header.stamp, cmd.stamp);
  EXPECT_EQ(ds_mx_msgs::StdPayloadCommand::COMMAND_START, cmd.command);
  ASSERT_EQ(3, cmd.config.size());

  EXPECT_EQ("detect_mode", cmd.config[0].key);
  EXPECT_EQ("mixed", cmd.config[0].value);

  EXPECT_EQ("force_depth", cmd.config[1].key);
  EXPECT_EQ("false", cmd.config[1].value);

  EXPECT_EQ("ping_rate", cmd.config[2].key);
  EXPECT_EQ("1", cmd.config[2].value);


  // tick (running)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::RUNNING;
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, underTest->tick(state));

  // tick (done)
  subtask->defaultTickReturn = ds_mx::TaskReturnCode::SUCCESS;
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, underTest->tick(state));

  // call on-stop
  state.header.stamp += ros::Duration(10);
  underTest->onStop(state);

  // look for a stop message
  DSMX_EXPECT_MESSAGE(cmd, cmd_msgs, 0.150);
  EXPECT_EQ(0, cmd_msgs.numMessages()); // there should be only one

  // check stop message
  EXPECT_EQ(state.header.stamp, cmd.stamp);
  EXPECT_EQ(ds_mx_msgs::StdPayloadCommand::COMMAND_STOP, cmd.command);
  ASSERT_EQ(0, cmd.config.size());

  // check to make sure the subtask was asked to do the right stuff
  EXPECT_EQ(1, subtask->onStartCalls);
  EXPECT_EQ(2, subtask->ticks);
  EXPECT_EQ(1, subtask->onStopCalls);

  EXPECT_FALSE(cmd_msgs.waitForMessage(0.025)); // wait 25ms for a message
}

} // namespace test
} // namespace sentry_mxtasks

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "rostest_sentry_run_mb");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(2); // use two threads
  spinner.start(); // returns immediately

  ds_mx::eventlog_disable();

  return RUN_ALL_TESTS();
}