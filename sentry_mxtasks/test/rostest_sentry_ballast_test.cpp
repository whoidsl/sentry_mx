/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/18/19.
//


#include <sentry_mxtasks/sentry_ballast_test.h>
#include "test_sentry_common.h"

#include <sentry_msgs/SentryJoystickEnum.h>
#include <sentry_msgs/SentryControllerEnum.h>
#include <sentry_msgs/SentryAllocationEnum.h>


namespace sentry_mxtasks {
namespace test {

// These primitives have very same-y google test classes.  Let's use a common base class
// and override the JSON description string.
class SentryBallastTestRostest : public SentryMxPrimitiveRostest<TaskBallastTest> {
 protected:
  virtual std::string getJSON() const {
    return R"( {
  "type": "sentry_ballasttest",
  "max_timeout": 0.1,
  "hold_altitude": true,
  "altitude": 50,
  "depth": 1000,
  "depth_floor": 1500
})";
  }
};

TEST_F(SentryBallastTestRostest, TestSendCmd) {

  // start, and expect a message
  underTest->onStart(state);

  // get the next message
  sentry_msgs::SentryMxGoal goal;
  DSMX_EXPECT_MESSAGE(goal, goal_msgs, 0.150);
  EXPECT_EQ(0, goal_msgs.numMessages()); // there should be only one

  // check the message that went out
  EXPECT_EQ(state.header.stamp, goal.stamp);

  EXPECT_EQ(sentry_msgs::SentryControllerEnum::BALLAST, goal.controller_mode);
  EXPECT_EQ(sentry_msgs::SentryAllocationEnum::VERTICAL_MODE, goal.allocator_mode);

  EXPECT_TRUE(goal.altitude.valid);
  EXPECT_DOUBLE_EQ(50.0, goal.altitude.value);

  EXPECT_TRUE(goal.depth_floor.valid);
  EXPECT_DOUBLE_EQ(1500, goal.depth_floor.value);

  // check everything else
  EXPECT_FALSE(goal.commanded_state.easting.valid);
  EXPECT_FALSE(goal.commanded_state.northing.valid);
  EXPECT_FALSE(goal.commanded_state.down.valid);
  EXPECT_FALSE(goal.commanded_state.surge_u.valid);
  EXPECT_FALSE(goal.commanded_state.sway_v.valid);
  EXPECT_FALSE(goal.commanded_state.heave_w.valid);
  EXPECT_FALSE(goal.commanded_state.roll.valid);
  EXPECT_FALSE(goal.commanded_state.pitch.valid);
  EXPECT_FALSE(goal.commanded_state.heading.valid);
  EXPECT_FALSE(goal.commanded_state.p.valid);
  EXPECT_FALSE(goal.commanded_state.q.valid);
  EXPECT_FALSE(goal.commanded_state.r.valid);

  // tick, expect nothing
  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // update state
  state.header.stamp += ros::Duration(0.101); // 1 ms later

  rc = underTest->tick(state);
  // surface joystick FAILS when it finishes
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, rc);

  // expect no additional messages
  EXPECT_FALSE(goal_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get an
}

TEST_F(SentryBallastTestRostest, TestSendDepthModeCmd) {

  // tweak our hold_altitude parameter
  EXPECT_FALSE(underTest->isRunning());
  underTest->getParameters().get<ds_mx::BoolParam>("hold_altitude").set(false);

  // start, and expect a message
  underTest->onStart(state);

  // get the next message
  sentry_msgs::SentryMxGoal goal;
  DSMX_EXPECT_MESSAGE(goal, goal_msgs, 0.150);
  EXPECT_EQ(0, goal_msgs.numMessages()); // there should be only one

  // Check the message that went out for correctness
  EXPECT_EQ(state.header.stamp, goal.stamp);

  EXPECT_EQ(sentry_msgs::SentryControllerEnum::BALLAST, goal.controller_mode);
  EXPECT_EQ(sentry_msgs::SentryAllocationEnum::VERTICAL_MODE, goal.allocator_mode);

  EXPECT_FALSE(goal.altitude.valid);
  EXPECT_FALSE(goal.depth_floor.valid);

  EXPECT_TRUE(goal.commanded_state.down.valid);
  EXPECT_DOUBLE_EQ(1000.0, goal.commanded_state.down.value);

  // check everything else
  EXPECT_FALSE(goal.commanded_state.easting.valid);
  EXPECT_FALSE(goal.commanded_state.northing.valid);
  EXPECT_FALSE(goal.commanded_state.surge_u.valid);
  EXPECT_FALSE(goal.commanded_state.sway_v.valid);
  EXPECT_FALSE(goal.commanded_state.heave_w.valid);
  EXPECT_FALSE(goal.commanded_state.roll.valid);
  EXPECT_FALSE(goal.commanded_state.pitch.valid);
  EXPECT_FALSE(goal.commanded_state.heading.valid);
  EXPECT_FALSE(goal.commanded_state.p.valid);
  EXPECT_FALSE(goal.commanded_state.q.valid);
  EXPECT_FALSE(goal.commanded_state.r.valid);

  // tick, expect nothing
  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // update state
  state.header.stamp += ros::Duration(0.101); // 1 ms later

  rc = underTest->tick(state);
  // surface joystick FAILS when it finishes
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, rc);

  // expect no additional messages
  EXPECT_FALSE(goal_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

TEST_F(SentryBallastTestRostest, TestCancel) {
  EXPECT_FALSE(underTest->isRunning());

  // start, and expect a message
  underTest->onStart(state);

  // get the next message
  sentry_msgs::SentryMxGoal goal;
  DSMX_EXPECT_MESSAGE(goal, goal_msgs, 0.150);
  EXPECT_EQ(0, goal_msgs.numMessages()); // there should be only one

  // tick, expect nothing
  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // deliver a cancel event
  ds_mx_msgs::MxEvent evt;
  evt.eventid = "/cancel/primitive";
  underTest->handleEvent(evt);

  // tick, expect failure
  rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, rc);
}

} // namespace test
} // namespace sentry_mxtasks


int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "rostest_sentryballasttest");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(2); // use two threads
  spinner.start(); // returns immediately

  ds_mx::eventlog_disable();

  return RUN_ALL_TESTS();
}
