/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 8/19/19.
//

#ifndef SENTRY_MXTASKS_TEST_SENTRY_COMMON_H
#define SENTRY_MXTASKS_TEST_SENTRY_COMMON_H

#include <gtest/gtest.h>
#include <ds_mxcore/test/ds_mock_compiler.h>
#include <ds_mxutils/ds_test_message_queue.h>

#include <mutex>
#include <condition_variable>

namespace sentry_mxtasks {
namespace test {

template <typename T>
class SentryMxPrimitiveTest : public ::testing::Test {
 protected:
  virtual std::string getJSON() const = 0;

  void SetUp() override {

    // prepare our compiler
    std::shared_ptr<ds_mx::MxMockCompiler> compiler = ds_mx::MxMockCompiler::create();
    // no need for subtasks, because primitive!

    // read the config
    Json::Value config;
    Json::Reader reader;
    reader.parse(getJSON(), config);

    // setup our test
    underTest = std::make_shared<T>();
    underTest->init(config, compiler);

    // fill in our test state with some basic values
    state.header.stamp = ros::Time::now();
    state.lat = 1;
    state.lon = 2;
    state.down = 1000;
    state.heading = 0.5;
    state.roll = 0.01;
    state.pitch = 0.02;
  }

  std::shared_ptr<T> underTest;
  ds_nav_msgs::NavState state;

};

template <typename T>
class SentryMxPrimitiveRostest : public SentryMxPrimitiveTest<T> {

 public:
  // we want basic subscription / node handle stuff to happen ONLY once at startup
  SentryMxPrimitiveRostest() : SentryMxPrimitiveTest<T>(), goal_msgs("sentry_cmd") {
    node_handle.reset(new ros::NodeHandle());
  }

 protected:
  void SetUp() override {
    // clear our message queue
    goal_msgs.SetUp();

    // Initialize our Task Under Test the usual way first
    SentryMxPrimitiveTest<T>::SetUp();

    // Then also run the ROS init function for our shiny new Task
    SentryMxPrimitiveTest<T>::underTest->init_ros(*node_handle);
  }

 protected:
  ds_mx::TestMessageQueue<sentry_msgs::SentryMxGoal> goal_msgs;

 private:
  std::unique_ptr<ros::NodeHandle> node_handle;
};

} // namespace test
} // namespace sentry_mxtasks

#endif //SENTRY_MXTASKS_TEST_SENTRY_COMMON_H
