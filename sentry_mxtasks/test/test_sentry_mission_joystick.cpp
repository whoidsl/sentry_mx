/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 8/19/19.
//

#include <sentry_mxtasks/sentry_mission_joystick.h>
#include "test_sentry_common.h"

#include <ds_control_msgs/JoystickEnum.h>
#include <sentry_msgs/SentryJoystickEnum.h>
#include <sentry_msgs/SentryControllerEnum.h>
#include <sentry_msgs/SentryAllocationEnum.h>


namespace sentry_mxtasks {
namespace test {

// These primitives have very same-y google test classes.  Let's use a common base class
// and override the JSON description string.
class SentryMissjoyTest : public SentryMxPrimitiveTest<TaskJoystickMission> {
protected:
virtual std::string getJSON() const {
  return R"( {
  "type": "sentry_missionjoy",
  "max_timeout": 0.1,
  "allocation": "flight",
  "thrust_fwd": 0.5,
  "thrust_down": 0.4,
  "thrust_hdg": -0.1
})";
}
};

TEST_F(SentryMissjoyTest, TestValidate) {
  // initial configuration is valid
  EXPECT_TRUE(underTest->validate());
}

TEST_F(SentryMissjoyTest, TestDisplay) {
  ds_mx_msgs::MissionDisplay display;

  underTest->getDisplay(state, display);

  ASSERT_EQ(1, display.elements.size());
  EXPECT_EQ(ds_mx_msgs::MissionElementDisplay::ROLE_JOYSTICK, display.elements[0].role);
  EXPECT_EQ("POINT (2.000000000 1.000000000)", display.elements[0].wellknowntext);
}

TEST_F(SentryMissjoyTest, TestBuildCmd) {
  sentry_msgs::SentryMxGoal goal = underTest->buildCmdMessage(state);

  // first, check our modes
  EXPECT_EQ(ds_control_msgs::JoystickEnum::MC, goal.joystick_mode);
  EXPECT_EQ(sentry_msgs::SentryControllerEnum::JOYSTICK, goal.controller_mode);
  EXPECT_EQ(sentry_msgs::SentryAllocationEnum::FLIGHT_MODE, goal.allocator_mode);

  // next, check our requested state
  EXPECT_TRUE(goal.commanded_state.surge_u.valid);
  EXPECT_DOUBLE_EQ(0.5, goal.commanded_state.surge_u.value);

  EXPECT_TRUE(goal.commanded_state.heave_w.valid);
  EXPECT_DOUBLE_EQ(0.4, goal.commanded_state.heave_w.value);

  EXPECT_TRUE(goal.commanded_state.r.valid);
  EXPECT_DOUBLE_EQ(-0.1, goal.commanded_state.r.value);

  // check everything else
  EXPECT_FALSE(goal.commanded_state.easting.valid);
  EXPECT_FALSE(goal.commanded_state.northing.valid);
  EXPECT_FALSE(goal.commanded_state.down.valid);
  EXPECT_FALSE(goal.commanded_state.sway_v.valid);
  EXPECT_FALSE(goal.commanded_state.roll.valid);
  EXPECT_FALSE(goal.commanded_state.pitch.valid);
  EXPECT_FALSE(goal.commanded_state.heading.valid);
  EXPECT_FALSE(goal.commanded_state.p.valid);
  EXPECT_FALSE(goal.commanded_state.q.valid);

  EXPECT_EQ(state.header.stamp, goal.stamp);
}

} // namespace test
} // namespace sentry_mxtasks