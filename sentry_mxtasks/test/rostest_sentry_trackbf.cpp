/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/17/19.
//

#include <ds_mxcore/ds_mxeventlog.h>
#include <sentry_mxtasks/sentry_trackbf.h>
#include "test_sentry_common.h"

#include <sentry_msgs/SentryJoystickEnum.h>
#include <sentry_msgs/SentryControllerEnum.h>
#include <sentry_msgs/SentryAllocationEnum.h>

#include <gtest/gtest.h>

namespace sentry_mxtasks {
namespace test {

// These primitives have very same-y google test classes.  Let's use a common base class
// and override the JSON description string.
class SentryTrackBfRostest : public SentryMxPrimitiveRostest<TaskTracklineBf> {
 protected:
  virtual std::string getJSON() const {
    return R"( {
  "type": "sentry_trackbf",
  "start_pt": "POINT(2.0001 1.0001) ",
  "end_pt": "POINT(2.0001 1.0002) ",
  "speed": 1.0,
  "altitude": 65,
  "envelope": 20.0,
  "gain": 4.0,
  "depthvel": 0.33,
  "depthacc": 0.07,
  "min_speed": 0.4,
  "alarm_timeout": 15.0,
  "depth_floor": 6000,
  "timeout_multiplier": 1.5,
  "timeout_increment": 45
})";
  }
};

TEST_F(SentryTrackBfRostest, TestSend) {
  // start, and expect a message
  underTest->onStart(state);

  // get the next message
  sentry_msgs::SentryMxGoal goal;
  DSMX_EXPECT_MESSAGE(goal, goal_msgs, 0.150);
  EXPECT_EQ(0, goal_msgs.numMessages()); // there should be only one

  // check the outgoing message
  EXPECT_EQ(sentry_msgs::SentryControllerEnum::SURVEY, goal.controller_mode);
  EXPECT_EQ(sentry_msgs::SentryAllocationEnum::FLIGHT_MODE, goal.allocator_mode);

  EXPECT_EQ(state.header.stamp, goal.stamp);
  EXPECT_DOUBLE_EQ(2.0001, goal.trackline_start.longitude);
  EXPECT_DOUBLE_EQ(1.0001, goal.trackline_start.latitude);
  EXPECT_DOUBLE_EQ(2.0001, goal.trackline_end.longitude);
  EXPECT_DOUBLE_EQ(1.0002, goal.trackline_end.latitude);
  EXPECT_TRUE(goal.commanded_state.surge_u.valid);
  EXPECT_DOUBLE_EQ(1.0, goal.commanded_state.surge_u.value);
  EXPECT_TRUE(goal.altitude.valid);
  EXPECT_DOUBLE_EQ(65.0, goal.altitude.value);
  EXPECT_TRUE(goal.depth_floor.valid);
  EXPECT_FLOAT_EQ(6000, goal.depth_floor.value);
  EXPECT_FLOAT_EQ(20.0, goal.bf_envelope);
  EXPECT_FLOAT_EQ(4.0, goal.bf_gain);
  EXPECT_FLOAT_EQ(0.33, goal.bf_depthvel);
  EXPECT_FLOAT_EQ(0.07, goal.bf_depthacc);
  EXPECT_FLOAT_EQ(0.4, goal.bf_min_speed);
  EXPECT_FLOAT_EQ(15.0, goal.bf_alarm_timeout);

  // tick, expect nothing
  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // update state
  state.header.stamp += ros::Duration(1.0);
  state.lat = 1.0002001; // approx 11mm over the line
  state.lon = 2.0001;

  rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::SUCCESS, rc);

  // expect no additional messages
  EXPECT_FALSE(goal_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

TEST_F(SentryTrackBfRostest, TestTimeout) {
  // start, and expect a message
  underTest->onStart(state);

  // get the next message
  sentry_msgs::SentryMxGoal goal;
  DSMX_EXPECT_MESSAGE(goal, goal_msgs, 0.150);
  EXPECT_EQ(0, goal_msgs.numMessages()); // there should be only one

  // check the outgoing message-- most of it is checked elsewhere, this is really
  // about whether timeouts work.
  EXPECT_EQ(sentry_msgs::SentryControllerEnum::SURVEY, goal.controller_mode);
  EXPECT_EQ(sentry_msgs::SentryAllocationEnum::FLIGHT_MODE, goal.allocator_mode);

  // tick, expect nothing
  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // update state
  state.header.stamp = underTest->timeoutTime() + ros::Duration(0, 1000000); // 1 ms past timeout

  rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, rc);

  // expect no additional messages
  EXPECT_FALSE(goal_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

TEST_F(SentryTrackBfRostest, TestCancel) {
  EXPECT_FALSE(underTest->isRunning());

  // start, and expect a message
  underTest->onStart(state);

  // get the next message
  sentry_msgs::SentryMxGoal goal;
  DSMX_EXPECT_MESSAGE(goal, goal_msgs, 0.150);
  EXPECT_EQ(0, goal_msgs.numMessages()); // there should be only one

  // tick, expect nothing
  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // deliver a cancel event
  ds_mx_msgs::MxEvent evt;
  evt.eventid = "/cancel/primitive";
  underTest->handleEvent(evt);

  // tick, expect failure
  rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, rc);
}

} // namespace test
} // namespace sentry_mxtasks

int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "rostest_sentrytrackbf");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(2); // use two threads
  spinner.start(); // returns immediately

  ds_mx::eventlog_disable();

  return RUN_ALL_TESTS();
}