/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/18/19.
//


#include <sentry_mxtasks/sentry_surface_joystick.h>
#include "test_sentry_common.h"

#include <sentry_msgs/SentryJoystickEnum.h>
#include <sentry_msgs/SentryControllerEnum.h>
#include <sentry_msgs/SentryAllocationEnum.h>


namespace sentry_mxtasks {
namespace test {

// These primitives have very same-y google test classes.  Let's use a common base class
// and override the JSON description string.
class SentrySurfjoyRostest : public SentryMxPrimitiveRostest<TaskJoystickSurface> {
 protected:
  virtual std::string getJSON() const {
    return R"( {
  "type": "sentry_surfjoy",
  "max_timeout": 0.1
})";
  }
};

TEST_F(SentrySurfjoyRostest, TestSend) {
  // start, and expect a message
  underTest->onStart(state);

  // get the next message
  sentry_msgs::SentryMxGoal goal;
  DSMX_EXPECT_MESSAGE(goal, goal_msgs, 0.150);
  EXPECT_EQ(0, goal_msgs.numMessages()); // there should be only one

  // check the resulting goal message
  EXPECT_EQ(sentry_msgs::SentryJoystickEnum::CONJOY, goal.joystick_mode);
  EXPECT_EQ(sentry_msgs::SentryControllerEnum::JOYSTICK, goal.controller_mode);

  EXPECT_EQ(state.header.stamp, goal.stamp);

  // tick, expect nothing
  ds_mx::TaskReturnCode rc = underTest->tick(state);
  EXPECT_EQ(ds_mx::TaskReturnCode::RUNNING, rc);

  // update state
  state.header.stamp += ros::Duration(0.101); // 1 ms later

  rc = underTest->tick(state);
  // surface joystick FAILS when it finishes
  EXPECT_EQ(ds_mx::TaskReturnCode::FAILED, rc);

  // expect no additional messages
  EXPECT_FALSE(goal_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any
}

} // namespace test
} // namespace sentry_mxtasks

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "rostest_sentrysurfjoy");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(2); // use two threads
  spinner.start(); // returns immediately

  ds_mx::eventlog_disable();

  return RUN_ALL_TESTS();
}
