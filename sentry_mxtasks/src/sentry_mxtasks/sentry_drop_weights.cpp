/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//


#include "sentry_mxtasks/sentry_drop_weights.h"

#include <ds_actuator_msgs/DropweightCmd.h>
#include <pluginlib/class_list_macros.h>

namespace sentry_mxtasks {

PayloadDropWeights::PayloadDropWeights() :
    drop_descent(params, "drop_descent", ds_mx::ParameterFlag::STATIC),
    burn_descent(params, "burn_descent", ds_mx::ParameterFlag::STATIC),
    drop_ascent(params,  "drop_ascent", ds_mx::ParameterFlag::STATIC),
    burn_ascent(params,  "burn_ascent", ds_mx::ParameterFlag::STATIC),
    delay_before_seconds(params, "delay_before_seconds", 0, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL),
    delay_after_seconds(params, "delay_after_seconds", 0, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL),
    repeat_seconds(params, "repeat_seconds", 0, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL),
    success_when_done(params, "success_when_done", false, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL)
{
  dropped = false;
}

void PayloadDropWeights::init_ros(ros::NodeHandle &nh) {
  ds_mx::MxPayloadTask::init_ros(nh);
  cmd_pub_ = nh.advertise<ds_actuator_msgs::DropweightCmd>("sentry_dropweight_cmd", 10);
}

bool PayloadDropWeights::validate() const {
  bool ret = true;

  if (! drop_descent.get() && !drop_ascent.get() && !burn_descent.get() && !burn_ascent.get()) {
    ROS_ERROR("PayloadDropWeights Task does not drop any weights!");
    ret = false;
  }

  if (delay_before_seconds.get() < 0) {
    ROS_ERROR_STREAM("DelayBeforeSeconds (" <<delay_before_seconds.get() <<") is less than zero!");
    ret = false;
  }

  if (delay_after_seconds.get() < 0) {
    ROS_ERROR_STREAM("DelayAfterSeconds (" <<delay_after_seconds.get() <<") is less than zero!");
    ret = false;
  }

  if ( (delay_after_seconds.get() > 0 || success_when_done.get()) && repeat_seconds.get() > 0) {
    ROS_ERROR_STREAM("DelayAfterSeconds (" << delay_after_seconds.get()
    <<") or success_when_done (" <<success_when_done.get()
    <<") suggest this task should exit, but repeat_seconds " <<repeat_seconds.get() <<" wants it to run forever");
    ret = false;
  }

  return ret;
}

void PayloadDropWeights::onStart(const ds_nav_msgs::NavState &state) {
  // MxPayloadTask::onStart is pure virtual; call ITS base class
  ds_mx::MxPayloadTask::onStart(state);

  drop_time = state.header.stamp + ros::Duration(delay_before_seconds.get());
  done_time = drop_time + ros::Duration(delay_after_seconds.get());

  // drop immediately if delay_before_seconds = 0
  if (delay_before_seconds.get() == 0) {
    _drop(state, " no delay set and task is starting");
  } else {
    // not sure this makes ANY sense.  We don't reload the weights
    dropped = false;
  }
}

void PayloadDropWeights::onStop(const ds_nav_msgs::NavState &state) {
  if (!dropped) {
    _drop(state, "weights not dropped prior to stop");
  }
  // MxPayloadTask::onStop is pure virtual; call ITS base class
  ds_mx::MxComplexTask::onStop(state);
}

ds_actuator_msgs::DropweightCmd PayloadDropWeights::buildCmdMessage(const ds_nav_msgs::NavState& state) const {
  ds_actuator_msgs::DropweightCmd ret;

  ret.stamp = state.header.stamp;

  ret.drop[ds_actuator_msgs::DropweightCmd::IDX_ASCENT] = drop_ascent.get();
  ret.burn[ds_actuator_msgs::DropweightCmd::IDX_ASCENT] = burn_ascent.get();
  ret.drop[ds_actuator_msgs::DropweightCmd::IDX_DESCENT] = drop_descent.get();
  ret.burn[ds_actuator_msgs::DropweightCmd::IDX_DESCENT] = burn_descent.get();

  return ret;
}

ds_mx::TaskReturnCode PayloadDropWeights::onTick(const ds_nav_msgs::NavState& state) {
  if (!dropped) {
    if (state.header.stamp >= drop_time) {
      _drop(state, "after requested delay");
      // this will not perform the done_time check until the next tick.
    }
  } else if (state.header.stamp >= done_time && success_when_done.get()) {
    // we're done, and we're supposed to return success when done
    return ds_mx::TaskReturnCode::SUCCESS;
  }

  // in all other cases, just keep running
  return ds_mx::TaskReturnCode::RUNNING;
}

ds_mx::TaskReturnCode PayloadDropWeights::subtaskDone(const ds_nav_msgs::NavState& state,
                                    ds_mx::TaskReturnCode subtaskCode) {
  if (dropped) {
    return subtaskCode;
  }

  _drop(state, "Subtask returned while waiting to drop");

  return subtaskCode;
}

void PayloadDropWeights::_drop(const ds_nav_msgs::NavState& state, const std::string& reason) {

  std::string weight;
  if (drop_descent.get()) weight += "drop_descent ";
  if (burn_descent.get()) weight += "burn_descent ";
  if (drop_ascent.get()) weight += "drop_ascent ";
  if (burn_ascent.get()) weight += "burn_ascent ";
  MX_LOG_EVENT_STREAM(this, MX_LOG_WEIGHTDROP, "Running " <<weight <<" because " <<reason);
  cmd_pub_.publish(buildCmdMessage(state));

  if (repeat_seconds.get() > 0) {
    // drop repeatedly; pretend we haven't dropped yet and reset the drop time
    dropped = false;
    drop_time = state.header.stamp + ros::Duration(repeat_seconds.get());
  } else {
    dropped = true;
    done_time = state.header.stamp + ros::Duration(delay_after_seconds.get());
  }
}

} // namespace sentry_mxtasks

PLUGINLIB_EXPORT_CLASS(sentry_mxtasks::PayloadDropWeights, ds_mx::MxTask)