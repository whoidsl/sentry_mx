/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//


#include "sentry_mxtasks/sentry_run_camera.h"

#include <pluginlib/class_list_macros.h>

namespace sentry_mxtasks {

PayloadRunCamera::PayloadRunCamera() : ds_mx_stdtask::StdPayload() {
}

void PayloadRunCamera::init_ros(ros::NodeHandle &nh) {
  ds_mx::MxComplexTask::init_ros(nh);

  cmd_pub = nh.advertise<ds_mx_msgs::StdPayloadCommand>("sentry_cam_cmd", 2, false);
}

bool PayloadRunCamera::validate() const {
  // payload run camera does not have any validation requirements
  return ds_mx::MxPayloadTask::validateSubtasks();
}

} // namespace sentry_mxtasks

PLUGINLIB_EXPORT_CLASS(sentry_mxtasks::PayloadRunCamera, ds_mx::MxTask)
