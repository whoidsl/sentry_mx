/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//

#include "sentry_mxtasks/sentry_trackbf.h"
#include <sentry_msgs/SentryMxGoal.h>

#include <sentry_msgs/SentryJoystickEnum.h>
#include <sentry_msgs/SentryControllerEnum.h>
#include <sentry_msgs/SentryAllocationEnum.h>
#include <ds_libtrackline/WktUtil.h>

#include <pluginlib/class_list_macros.h>

namespace sentry_mxtasks {

TaskTracklineBf::TaskTracklineBf() :
    start_pt(params, "start_pt", ds_mx::ParameterFlag::DYNAMIC),
    end_pt(params, "end_pt", ds_mx::ParameterFlag::DYNAMIC),
    speed(params, "speed", ds_mx::ParameterFlag::DYNAMIC),
    altitude(params, "altitude", ds_mx::ParameterFlag::DYNAMIC),
    depth_floor(params, "depth_floor", ds_mx::ParameterFlag::DYNAMIC),
    envelope(params, "envelope", ds_mx::ParameterFlag::DYNAMIC),
    gain(params, "gain", ds_mx::ParameterFlag::DYNAMIC),
    depthvel(params, "depthvel", ds_mx::ParameterFlag::DYNAMIC),
    depthacc(params, "depthacc", ds_mx::ParameterFlag::DYNAMIC),
    min_speed(params, "min_speed", ds_mx::ParameterFlag::DYNAMIC),
    alarm_timeout(params, "alarm_timeout", ds_mx::ParameterFlag::DYNAMIC),
    timeout_multiplier(params, "timeout_multiplier", 2.0, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL),
    timeout_increment(params, "timeout_increment", 60.0, ds_mx::ParameterFlag::STATIC | ds_mx::ParameterFlag::OPTIONAL),
    cancel(events, "cancel", "/cancel/primitive", &TaskTracklineBf::event_detected, this),
    trackline(0,0,0,0) {

}

void TaskTracklineBf::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxPrimitiveTask::init(config, compiler);

  // initialize our trackline
  updateTrackline();
}
void TaskTracklineBf::init_ros(ros::NodeHandle& nh) {
  cmd_pub_ = nh.advertise<sentry_msgs::SentryMxGoal>("sentry_cmd", 10);
}
bool TaskTracklineBf::validate() const {
  // Not really sure what to do here; pretty much all tracklines are valid
  // Speed should be positive though?
  if (speed.get() < 0) {
    ROS_ERROR_STREAM("Trackline speed must be non-negative, got " <<speed.get());
    return false;
  }
  return true;
}

void TaskTracklineBf::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) {
  // access our start/end
  const auto& line_start = trackline.getStartLL();
  const auto& line_end = trackline.getEndLL();

  ds_trackline::Trackline connectingLine(state.lon, state.lat, line_start(0), line_start(1));

  ds_mx_msgs::MissionElementDisplay line1;
  line1.role = ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE_CONNECTING;
  line1.wellknowntext = connectingLine.getWktLL();
  std::copy(uuid.begin(), uuid.end(), line1.task_uuid.begin());
  display.elements.push_back(line1);

  // account for time
  state.header.stamp += ros::Duration(connectingLine.getLength() / speed.get());

  // draw the trackline itself
  ds_mx_msgs::MissionElementDisplay line2;
  line2.role = ds_mx_msgs::MissionElementDisplay::ROLE_TRACKLINE;
  line2.wellknowntext = trackline.getWktLL();
  std::copy(uuid.begin(), uuid.end(), line2.task_uuid.begin());
  display.elements.push_back(line2);

  // update state to nominal location
  state.lon = line_end(0);
  state.lat = line_end(1);
  state.header.stamp += ros::Duration(trackline.getLength() / speed.get());
}

void TaskTracklineBf::parameterChanged(const ds_nav_msgs::NavState &state) {
  updateTrackline();
  cmd_pub_.publish(buildCmdMessage(state));
}

void TaskTracklineBf::onStart(const ds_nav_msgs::NavState &state) {
  ds_mx::MxPrimitiveTask::onStart(state);
  cmd_pub_.publish(buildCmdMessage(state));
}

ds_mx::TaskReturnCode TaskTracklineBf::onTick(const ds_nav_msgs::NavState& state) {

  // check for doneness
  ds_trackline::Trackline::VectorEN along_across = trackline.lonlat_to_trackframe(state.lon, state.lat);
  if (along_across[0] > 0) {
    // We're done!
    //ROS_WARN_STREAM("Finished trackline!");
    return ds_mx::SUCCESS;
  }

  // 0-length tracklines are buggy-- heading is ill-defined-- and should return immediately
  if (trackline.getLength() < 0.001) {
    ROS_ERROR_STREAM("Trackline too short (<1mm), skipping...");
    return ds_mx::SUCCESS;
  }

  return ds_mx::TaskReturnCode::RUNNING;
}

bool TaskTracklineBf::event_detected(ds_mx_msgs::MxEvent event) {
  next_tick_return_code = ds_mx::TaskReturnCode::FAILED;
  return false;
}

void TaskTracklineBf::resetTimeout(const ros::Time& now) {
  if (params->anyChanged()) {
    updateTrackline();
    params->resetChanged();
  }
  // don't divide by zero
  double expected_time=0;
  if (speed.get() >= 0.001) {
    expected_time = trackline.getLength() / speed.get();
  }

  timeout = now + ros::Duration(expected_time * timeout_multiplier.get() + timeout_increment.get());
}


void TaskTracklineBf::updateTrackline() {
  ds_mx::GeoPoint start = start_pt.get();
  ds_mx::GeoPoint end = end_pt.get();
  trackline = ds_trackline::Trackline(start.x, start.y, end.x, end.y);
}

sentry_msgs::SentryMxGoal TaskTracklineBf::buildCmdMessage(const ds_nav_msgs::NavState& state) const {
  sentry_msgs::SentryMxGoal goal;

  goal.stamp = state.header.stamp;

  // set the desired control modes
  goal.controller_mode = sentry_msgs::SentryControllerEnum::SURVEY;
  goal.allocator_mode = sentry_msgs::SentryAllocationEnum::FLIGHT_MODE;

  // set the speed and altitude goals
  goal.commanded_state.surge_u.valid = true;
  goal.commanded_state.surge_u.value = speed.get();

  goal.altitude.valid = true;
  goal.altitude.value = altitude.get();

  goal.depth_floor.valid = true;
  goal.depth_floor.value = depth_floor.get();

  // set the trackline
  auto start_lonlat = trackline.getStartLL();
  auto end_lonlat = trackline.getEndLL();

  goal.trackline_start.longitude = start_lonlat[0];
  goal.trackline_start.latitude = start_lonlat[1];

  goal.trackline_end.longitude = end_lonlat[0];
  goal.trackline_end.latitude = end_lonlat[1];

  goal.bf_envelope = envelope.get();
  goal.bf_gain = gain.get();
  goal.bf_depthvel = depthvel.get();
  goal.bf_depthacc = depthacc.get();
  goal.bf_min_speed = min_speed.get();
  goal.bf_alarm_timeout = alarm_timeout.get();

  return goal;
}

} //namespace sentry_mxtasks

PLUGINLIB_EXPORT_CLASS(sentry_mxtasks::TaskTracklineBf, ds_mx::MxTask)
