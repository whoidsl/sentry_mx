/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//

#include "sentry_mxtasks/sentry_surface_joystick.h"

#include <sentry_msgs/SentryMxGoal.h>
#include <sentry_msgs/SentryJoystickEnum.h>
#include <sentry_msgs/SentryControllerEnum.h>
#include <sentry_msgs/SentryAllocationEnum.h>

#include <ds_libtrackline/WktUtil.h>

#include <pluginlib/class_list_macros.h>

namespace sentry_mxtasks {

void TaskJoystickSurface::init(const Json::Value &config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxPrimitiveTask::init(config, compiler);
  timeout_code = ds_mx::TaskReturnCode::FAILED;
}

void TaskJoystickSurface::init_ros(ros::NodeHandle &nh) {
  cmd_pub_ = nh.advertise<sentry_msgs::SentryMxGoal>("sentry_cmd", 10);
}

bool TaskJoystickSurface::validate() const {
  // Surface joystick does not need to return
  return true;
}

void TaskJoystickSurface::getDisplay(ds_nav_msgs::NavState &state, ds_mx_msgs::MissionDisplay &display) {
  // pretty much the same as idle
  state.header.stamp += ros::Duration(max_timeout_sec.get());

  ds_mx_msgs::MissionElementDisplay element;
  element.role = ds_mx_msgs::MissionElementDisplay::ROLE_JOYSTICK;
  element.wellknowntext = ds_trackline::wktPointLL(state.lon, state.lat);
  std::copy(uuid.begin(), uuid.end(), element.task_uuid.begin());
  display.elements.push_back(element);
}

void TaskJoystickSurface::onStart(const ds_nav_msgs::NavState &state) {
  ds_mx::MxPrimitiveTask::onStart(state);
  cmd_pub_.publish(buildCmdMessage(state));
}

ds_mx::TaskReturnCode TaskJoystickSurface::onTick(const ds_nav_msgs::NavState &state) {
  return ds_mx::TaskReturnCode::RUNNING;
}

sentry_msgs::SentryMxGoal TaskJoystickSurface::buildCmdMessage(const ds_nav_msgs::NavState& state) const {
  sentry_msgs::SentryMxGoal goal;

  goal.stamp = state.header.stamp;

  goal.joystick_mode = sentry_msgs::SentryJoystickEnum::CONJOY;
  goal.controller_mode = sentry_msgs::SentryControllerEnum::JOYSTICK;
  goal.allocator_mode = sentry_msgs::SentryAllocationEnum::IDLE_MODE;

  return goal;
}

} // namespace sentry_mxtasks

PLUGINLIB_EXPORT_CLASS(sentry_mxtasks::TaskJoystickSurface, ds_mx::MxTask)
