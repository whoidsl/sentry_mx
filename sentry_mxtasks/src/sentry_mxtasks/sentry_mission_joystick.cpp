/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//


#include "sentry_mxtasks/sentry_mission_joystick.h"

#include <sentry_msgs/SentryMxGoal.h>
#include <ds_control_msgs/JoystickEnum.h>
#include <sentry_msgs/SentryJoystickEnum.h>
#include <sentry_msgs/SentryControllerEnum.h>
#include <sentry_msgs/SentryAllocationEnum.h>

#include <ds_libtrackline/WktUtil.h>
#include <pluginlib/class_list_macros.h>

#include <boost/algorithm/string/case_conv.hpp>
#include <ds_control_msgs/JoystickEnum.h>

namespace sentry_mxtasks {

TaskJoystickMission::TaskJoystickMission() : allocation(params, "allocation", ds_mx::ParameterFlag::DYNAMIC),
                                             thrust_fwd(params, "thrust_fwd", 0, ds_mx::ParameterFlag::DYNAMIC),
                                             thrust_down(params, "thrust_down", 0, ds_mx::ParameterFlag::DYNAMIC),
                                             thrust_hdg(params, "thrust_hdg", 0, ds_mx::ParameterFlag::DYNAMIC),
                                             cancel(events, "cancel", "/cancel/primitive", &TaskJoystickMission::event_detected, this)
{ /* do nothing */ }

void TaskJoystickMission::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxPrimitiveTask::init(config, compiler);
  timeout_code = ds_mx::TaskReturnCode::SUCCESS;
}
void TaskJoystickMission::init_ros(ros::NodeHandle& nh) {
  cmd_pub_ = nh.advertise<sentry_msgs::SentryMxGoal>("sentry_cmd", 10);
}

bool TaskJoystickMission::validate() const {
  // Mission joystick can have no timeout (e.g., on surface, etc)
  return true;
}

void TaskJoystickMission::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) {
  // pretty much the same as idle
  state.header.stamp += ros::Duration(max_timeout_sec.get());

  ds_mx_msgs::MissionElementDisplay element;
  element.role = ds_mx_msgs::MissionElementDisplay::ROLE_JOYSTICK;
  element.wellknowntext = ds_trackline::wktPointLL(state.lon, state.lat);
  std::copy(uuid.begin(), uuid.end(), element.task_uuid.begin());
  display.elements.push_back(element);
}

void TaskJoystickMission::onStart(const ds_nav_msgs::NavState &state) {
  ds_mx::MxPrimitiveTask::onStart(state);
  cmd_pub_.publish(buildCmdMessage(state));
}

ds_mx::TaskReturnCode TaskJoystickMission::onTick(const ds_nav_msgs::NavState& state) {
  // will automagically end on timeout
  return ds_mx::TaskReturnCode::RUNNING;
}

bool TaskJoystickMission::event_detected(ds_mx_msgs::MxEvent event) {
  next_tick_return_code = ds_mx::TaskReturnCode::FAILED;
  return false;
}

void TaskJoystickMission::parameterChanged(const ds_nav_msgs::NavState& state) {
  // re-publish our state
  cmd_pub_.publish(buildCmdMessage(state));
}

sentry_msgs::SentryMxGoal TaskJoystickMission::buildCmdMessage(const ds_nav_msgs::NavState& state) const {
  sentry_msgs::SentryMxGoal goal;

  goal.stamp = state.header.stamp;

  goal.joystick_mode = ds_control_msgs::JoystickEnum::MC;
  goal.controller_mode = sentry_msgs::SentryControllerEnum::JOYSTICK;

  std::string lower_alloc(allocation.get());
  boost::algorithm::to_lower(lower_alloc);

  // set the allocation
  if (lower_alloc == "flight") {
    goal.allocator_mode = sentry_msgs::SentryAllocationEnum::FLIGHT_MODE;
  } else if (lower_alloc == "rov") {
    goal.allocator_mode = sentry_msgs::SentryAllocationEnum::ROV_MODE;
  } else if (lower_alloc == "vertical") {
    goal.allocator_mode = sentry_msgs::SentryAllocationEnum::VERTICAL_MODE;
  } else if (lower_alloc == "idle") {
    goal.allocator_mode = sentry_msgs::SentryAllocationEnum::IDLE_MODE;
  } else {
    ROS_ERROR_STREAM("Unknown allocation mode \"" <<goal.allocator_mode <<"\"");
    goal.allocator_mode = sentry_msgs::SentryAllocationEnum::IDLE_MODE;
  }

  goal.commanded_state.surge_u.valid = true;
  goal.commanded_state.surge_u.value = thrust_fwd.get();

  goal.commanded_state.heave_w.valid = true;
  goal.commanded_state.heave_w.value = thrust_down.get();

  goal.commanded_state.r.valid = true;
  goal.commanded_state.r.value = thrust_hdg.get();


  return goal;
}

} //namespace sentry_mxtasks

PLUGINLIB_EXPORT_CLASS(sentry_mxtasks::TaskJoystickMission, ds_mx::MxTask)
