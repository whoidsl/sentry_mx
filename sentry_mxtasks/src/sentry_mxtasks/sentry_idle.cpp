/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/12/19.
//

#include "sentry_mxtasks/sentry_idle.h"

#include <sentry_msgs/SentryMxGoal.h>
#include <sentry_msgs/SentryJoystickEnum.h>
#include <sentry_msgs/SentryControllerEnum.h>
#include <sentry_msgs/SentryAllocationEnum.h>

#include <ds_libtrackline/WktUtil.h>

#include <pluginlib/class_list_macros.h>

namespace sentry_mxtasks {

TaskIdle::TaskIdle() : ds_mx::MxPrimitiveTask(),
                       cancel(events, "cancel", "/cancel/primitive", &TaskIdle::event_detected, this) {}


void TaskIdle::init(const Json::Value& config, ds_mx::MxCompilerPtr compiler) {
  ds_mx::MxPrimitiveTask::init(config, compiler);
  timeout_code = ds_mx::TaskReturnCode::SUCCESS;
}
void TaskIdle::init_ros(ros::NodeHandle& nh) {
  cmd_pub_ = nh.advertise<sentry_msgs::SentryMxGoal>("sentry_cmd", 10);
}

bool TaskIdle::validate() const {
  // Idle task must define a timeout
  return max_timeout_sec.get() > 0;
}

void TaskIdle::getDisplay(ds_nav_msgs::NavState& state, ds_mx_msgs::MissionDisplay& display) {

  state.header.stamp += ros::Duration(max_timeout_sec.get());

  ds_mx_msgs::MissionElementDisplay element;
  element.role = ds_mx_msgs::MissionElementDisplay::ROLE_IDLE;
  element.wellknowntext = ds_trackline::wktPointLL(state.lon, state.lat);
  std::copy(uuid.begin(), uuid.end(), element.task_uuid.begin());
  display.elements.push_back(element);
}

sentry_msgs::SentryMxGoal TaskIdle::buildCmdMessage(const ds_nav_msgs::NavState& state) const {
  sentry_msgs::SentryMxGoal goal;

  goal.stamp = state.header.stamp;
  goal.joystick_mode = sentry_msgs::SentryJoystickEnum::NONE;
  goal.controller_mode = sentry_msgs::SentryControllerEnum::NONE;
  goal.allocator_mode = sentry_msgs::SentryAllocationEnum::IDLE_MODE;

  return goal;
}

void TaskIdle::onStart(const ds_nav_msgs::NavState &state) {
  // call the base class
  ds_mx::MxPrimitiveTask::onStart(state);

  // set the IDLE controller as requested
  cmd_pub_.publish(buildCmdMessage(state));
}

ds_mx::TaskReturnCode TaskIdle::onTick(const ds_nav_msgs::NavState& state) {
  return ds_mx::TaskReturnCode::RUNNING;
}

bool TaskIdle::event_detected(ds_mx_msgs::MxEvent event) {
  next_tick_return_code = ds_mx::TaskReturnCode::FAILED;
  return false;
}

} //namespace sentry_mxtasks

PLUGINLIB_EXPORT_CLASS(sentry_mxtasks::TaskIdle, ds_mx::MxTask)
