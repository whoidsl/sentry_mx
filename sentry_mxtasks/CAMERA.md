# Camera Support

The rush to get this all working resulted in accidentally implementing the
MC camera interface instead of the camera driver interface.  We need to take
the "CAM [exp] [framerate] [gain]" directives of MC and convert them to the
"WEXP", "WGAIN", "WMODE", etc that the camera driver can actually use.

## Defaults:

Camera driver default:

* Exposure 15
* Gain 0
* Framerate 1.0

MC decktest:

* Exposure 10
* Gain 12
* Framerate 0.3

## How does MC actually set the camera, anyway:

```
main::send_imaging_stack_cmd("WMODE 0");
main::wait("00:00:01", $mynum, "0");
main::send_imaging_stack_cmd("WMODE 0");
main::wait("00:00:01", $mynum, "0");
main::send_imaging_stack_cmd("WEXP $cam_exposure");
main::wait("00:00:01", $mynum, "0");
main::send_imaging_stack_cmd("WREP $cam_rep");
main::wait("00:00:01", $mynum, "0");
main::send_imaging_stack_cmd("WGAIN $cam_gain");
main::wait("00:00:01", $mynum, "0");
main::send_imaging_stack_cmd("WEXP $cam_exposure");
main::wait("00:00:01", $mynum, "0");
main::send_imaging_stack_cmd("WREP $cam_rep");
main::wait("00:00:01", $mynum, "0");
main::send_imaging_stack_cmd("WGAIN $cam_gain");
main::wait("00:00:01", $mynum, "0");
main::send_imaging_stack_cmd("WMODE 1");
main::wait("00:00:01", $mynum, "0");
main::send_imaging_stack_cmd("WMODE 1");
```
