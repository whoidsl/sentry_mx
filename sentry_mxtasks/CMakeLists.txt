cmake_minimum_required(VERSION 2.8.3)
project(sentry_mxtasks)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  ds_mx_msgs
  ds_actuator_msgs
  ds_control_msgs
  ds_mxcore
  ds_mxutils
  ds_mx_stdtask
  sentry_msgs
  ds_libtrackline
)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)


## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend tag for "message_generation"
##   * add a build_depend and a exec_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependency has been pulled in
##     but can be declared for certainty nonetheless:
##     * add a exec_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
# add_message_files(
#   FILES
#   Message1.msg
#   Message2.msg
# )

## Generate services in the 'srv' folder
# add_service_files(
#   FILES
#   Service1.srv
#   Service2.srv
# )

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
# generate_messages(
#   DEPENDENCIES
#   ds_mx_msgs
# )

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

## To declare and build dynamic reconfigure parameters within this
## package, follow these steps:
## * In the file package.xml:
##   * add a build_depend and a exec_depend tag for "dynamic_reconfigure"
## * In this file (CMakeLists.txt):
##   * add "dynamic_reconfigure" to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * uncomment the "generate_dynamic_reconfigure_options" section below
##     and list every .cfg file to be processed

## Generate dynamic reconfigure parameters in the 'cfg' folder
# generate_dynamic_reconfigure_options(
#   cfg/DynReconf1.cfg
#   cfg/DynReconf2.cfg
# )

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if your package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS include
  LIBRARIES sentry_mxtasks
  CATKIN_DEPENDS ds_mx_msgs ds_mxcore ds_mxutils ds_mx_stdtask ds_libtrackline ds_actuator_msgs ds_control_msgs sentry_msgs
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ library
add_library(${PROJECT_NAME}
  src/${PROJECT_NAME}/sentry_idle.cpp
  include/${PROJECT_NAME}/sentry_idle.h
  src/${PROJECT_NAME}/sentry_surface_joystick.cpp
  include/${PROJECT_NAME}/sentry_surface_joystick.h
  src/${PROJECT_NAME}/sentry_mission_joystick.cpp
  include/${PROJECT_NAME}/sentry_mission_joystick.h
  src/${PROJECT_NAME}/sentry_trackbf.cpp
  include/${PROJECT_NAME}/sentry_trackbf.h
  src/${PROJECT_NAME}/sentry_ballast_test.cpp
  include/${PROJECT_NAME}/sentry_ballast_test.h
  src/${PROJECT_NAME}/sentry_drop_weights.cpp
  include/${PROJECT_NAME}/sentry_drop_weights.h
  src/${PROJECT_NAME}/sentry_run_mb.cpp
  include/${PROJECT_NAME}/sentry_run_mb.h
  src/${PROJECT_NAME}/sentry_run_camera.cpp
  include/${PROJECT_NAME}/sentry_run_camera.h
)

## Add cmake target dependencies of the library
## as an example, code may need to be generated before libraries
## either from message generation or dynamic reconfigure
add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(${PROJECT_NAME} ${catkin_LIBRARIES})

## Declare a C++ executable
## With catkin_make all packages are built within a single CMake context
## The recommended prefix ensures that target names across packages don't collide
# add_executable(${PROJECT_NAME}_node src/sentry_mxtasks_node.cpp)

## Rename C++ executable without prefix
## The above recommended prefix causes long target names, the following renames the
## target back to the shorter version for ease of user use
## e.g. "rosrun someones_pkg node" instead of "rosrun someones_pkg someones_pkg_node"
# set_target_properties(${PROJECT_NAME}_node PROPERTIES OUTPUT_NAME node PREFIX "")

## Add cmake target dependencies of the executable
## same as for the library above
# add_dependencies(${PROJECT_NAME}_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
# target_link_libraries(${PROJECT_NAME}_node
#   ${catkin_LIBRARIES}
# )

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark executables and/or libraries for installation
# install(TARGETS ${PROJECT_NAME} ${PROJECT_NAME}_node
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark cpp header files for installation
# install(DIRECTORY include/${PROJECT_NAME}/
#   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
#   FILES_MATCHING PATTERN "*.h"
#   PATTERN ".svn" EXCLUDE
# )

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
catkin_add_gtest(${PROJECT_NAME}-test test/test_sentrymx_main.cpp
        test/test_sentry_common.h
        test/test_sentry_idle.cpp
        test/test_sentry_ballast_test.cpp
        test/test_sentry_mission_joystick.cpp
        test/test_sentry_surface_joystick.cpp
        test/test_sentry_trackbf.cpp
        test/test_sentry_drop_weights.cpp)
if(TARGET ${PROJECT_NAME}-test)
  target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
endif()

# We're using rostest to run functional tests, which are subtly different than the core unit tests
if (CATKIN_ENABLE_TESTING)
  find_package(rostest REQUIRED)
  add_rostest_gtest(rostest_sentry_trackbf
          test/rostest_sentry_trackbf.test
          test/test_sentry_common.h
          test/rostest_sentry_trackbf.cpp)
  target_link_libraries(rostest_sentry_trackbf ${PROJECT_NAME})

  add_rostest_gtest(rostest_sentry_surfjoy
          test/rostest_sentry_surface_joystick.test
          test/test_sentry_common.h
          test/rostest_sentry_surface_joystick.cpp)
  target_link_libraries(rostest_sentry_surfjoy ${PROJECT_NAME})

  add_rostest_gtest(rostest_sentry_missjoy
          test/rostest_sentry_mission_joystick.test
          test/test_sentry_common.h
          test/rostest_sentry_mission_joystick.cpp)
  target_link_libraries(rostest_sentry_missjoy ${PROJECT_NAME})

  add_rostest_gtest(rostest_sentry_idle
          test/rostest_sentry_idle.test
          test/test_sentry_common.h
          test/rostest_sentry_idle.cpp)
  target_link_libraries(rostest_sentry_idle ${PROJECT_NAME})

  add_rostest_gtest(rostest_sentry_ballasttest
          test/rostest_sentry_ballast_test.test
          test/test_sentry_common.h
          test/rostest_sentry_ballast_test.cpp)
  target_link_libraries(rostest_sentry_ballasttest ${PROJECT_NAME})

  add_rostest_gtest(rostest_sentry_drop_weights
          test/rostest_sentry_drop_weights.test
          test/rostest_sentry_drop_weights.cpp)
  target_link_libraries(rostest_sentry_drop_weights ${PROJECT_NAME})

  add_rostest_gtest(rostest_sentry_run_camera
          test/rostest_sentry_run_camera.test
          test/rostest_sentry_run_camera.cpp)
  target_link_libraries(rostest_sentry_run_camera ${PROJECT_NAME})

  add_rostest_gtest(rostest_sentry_run_multibeam
          test/rostest_sentry_run_multibeam.test
          test/rostest_sentry_run_multibeam.cpp)
  target_link_libraries(rostest_sentry_run_multibeam ${PROJECT_NAME})

endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
