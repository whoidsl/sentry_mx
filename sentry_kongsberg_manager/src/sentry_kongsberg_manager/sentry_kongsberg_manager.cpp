/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/10/19.
//

#include "sentry_kongsberg_manager/sentry_kongsberg_manager.h"
#include <sentry_msgs/PWRCmd.h>
#include "sail_grd/pwr_lookup.h"

#include <ds_kongsberg_msgs/PingCmd.h>
#include <ds_kongsberg_msgs/SettingsCmd.h>

namespace sentry_mx {

SentryKongsbergManager::SentryKongsbergManager() {
  poweroff_timeout = 300;
}

SentryKongsbergManager::SentryKongsbergManager(int argc, char* argv[], const std::string& name) {
  poweroff_timeout = 300;
}

SentryKongsbergManager::~SentryKongsbergManager() = default;

void SentryKongsbergManager::setupParameters() {
  ds_base::DsProcess::setupParameters();
  ros::NodeHandle nh = nodeHandle();
  nh.getParam(ros::this_node::getName() + "/poweroff_timeout", poweroff_timeout);


  std::string power_name;
  if (!(nh.getParam(ros::this_node::getName() + "/power_name", power_name))) {
    ROS_FATAL_STREAM("MUST specify power name for multibeam!");
    ROS_BREAK();
  }
  power_addrs.clear();
  power_addrs.push_back(grd_util::get_address(power_name));
}

void SentryKongsbergManager::setupSubscriptions() {
  ds_base::DsProcess::setupSubscriptions();
  ros::NodeHandle nh = nodeHandle();

  cmd_sub_ = nh.subscribe("sentry_mb_cmd", 10,
      &SentryKongsbergManager::_cmd_callback, this);
}

void SentryKongsbergManager::setupTimers() {
  ds_base::DsProcess::setupTimers();
  ros::NodeHandle nh = nodeHandle();
  poweroff_timer = nh.createTimer(ros::Duration(poweroff_timeout),
      &SentryKongsbergManager::_shutdown_callback, this, true, false);
}

void SentryKongsbergManager::setupServices() {
  ds_base::DsProcess::setupServices();
  ros::NodeHandle nh = nodeHandle();
  pwr_srv_ = nh.serviceClient<sentry_msgs::PWRCmd>("pwr_service");

  mb_ping_srv_ = nh.serviceClient<ds_kongsberg_msgs::PingCmd>("mb_ping_service");
  mb_config_srv_ = nh.serviceClient<ds_kongsberg_msgs::SettingsCmd>("mb_settings_service");
}

void SentryKongsbergManager::_cmd_callback(const ds_mx_msgs::StdPayloadCommand& cmd) {
  switch(cmd.command) {
    case ds_mx_msgs::StdPayloadCommand::COMMAND_START:
      ROS_INFO_STREAM("Starting Kongsberg...");
      ROS_INFO_STREAM("    powering on multibeam");
      _cancel_shutdown_timer();
      setPower(true);
      _send_ping_cmd(ds_kongsberg_msgs::PingCmd::Request::PING_START);
      _send_config(cmd);
      break;

    case ds_mx_msgs::StdPayloadCommand::COMMAND_STOP:
      ROS_INFO_STREAM("Stopping multibeam...");
      _start_shutdown_timer();
      _send_ping_cmd(ds_kongsberg_msgs::PingCmd::Request::PING_STOP);
      break;

    case ds_mx_msgs::StdPayloadCommand::COMMAND_CONFIGONLY:
      ROS_INFO_STREAM("Updating multibeam config...");
      _cancel_shutdown_timer();
      _send_config(cmd);
      break;

    default:
      ROS_ERROR_STREAM("Unsupported standard payload command " <<cmd.command);
  }

}

void  SentryKongsbergManager::_shutdown_callback(const ros::TimerEvent& evt) {
  setPower(false);
}

void  SentryKongsbergManager::_start_shutdown_timer() {
  poweroff_timer.stop();
  poweroff_timer.start();
}

void  SentryKongsbergManager::_cancel_shutdown_timer() {
  poweroff_timer.stop();
}

void SentryKongsbergManager::setPower(bool on) {

  for (auto addr : this->power_addrs) {
    sentry_msgs::PWRCmd cmd;
    cmd.request.address = addr;
    if (on) {
      cmd.request.command = sentry_msgs::PWRCmd::Request::PWR_CMD_ON;
    } else {
      cmd.request.command = sentry_msgs::PWRCmd::Request::PWR_CMD_OFF;
    }

    if (!pwr_srv_.call(cmd)) {
      ROS_ERROR_STREAM("Unable to set power state of address: " <<addr);
    }
  }
}

void SentryKongsbergManager::_send_config(const ds_mx_msgs::StdPayloadCommand& cmd) {
  for (auto param : cmd.config) {
    ds_kongsberg_msgs::SettingsCmd msg;

    msg.request.setting_name = param.key;
    msg.request.setting_value = param.value;

    if (!mb_config_srv_.call(msg)) {
      ROS_ERROR_STREAM("Service call to set parameter " <<param.key <<" FAILED!");
    } else {
      ROS_INFO_STREAM("Key " <<param.key <<" --> " <<msg.response.command_sent);
    }
  }
}

void SentryKongsbergManager::_send_ping_cmd(uint8_t cmd) {
  ds_kongsberg_msgs::PingCmd msg;
  msg.request.ping = cmd;
  if (!mb_ping_srv_.call(msg)) {
    ROS_ERROR_STREAM("Unable to send PING command to Kongsberg!");
  } else {
    ROS_INFO_STREAM("Send ping command! Go action string: " <<msg.response.action);
  }
}

};
