
# TODO

* Add support to enable/disable lasers as parameters in the mission.  SentryCamManager.
* Rewrite SentryCamManager for new camera driver

# Existing tasks:

## Primitive

* TaskIdle
* TaskBallastTest
* TaskJoystickMission
* TaskJoystickSurface
* TaskTracklineBf

## Payload

* PayloadDropWeights
* PayloadRunCamera
* PayloadRunMultibeam