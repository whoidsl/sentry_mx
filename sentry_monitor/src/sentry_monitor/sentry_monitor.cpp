/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/23/19.
//

#include <sentry_monitor/sentry_monitor.h>

namespace sentry_monitor {

SentryMonitor::SentryMonitor() : ds_base::DsProcess(),
                                 main_humidity("main_humidity",
                                               ds_mxutils::OPTIONAL,
                                               ds_mxutils::REQUIRED,
                                               ds_mxutils::REQUIRED),
                                 main_temperature("main_temperature",
                                                  ds_mxutils::OPTIONAL,
                                                  ds_mxutils::REQUIRED,
                                                  ds_mxutils::REQUIRED),
                                 main_pressure("main_pressure",
                                               ds_mxutils::OPTIONAL,
                                               ds_mxutils::REQUIRED,
                                               ds_mxutils::REQUIRED),
                                 bat_humidity("bat_humidity",
                                              ds_mxutils::OPTIONAL,
                                              ds_mxutils::REQUIRED,
                                              ds_mxutils::REQUIRED),
                                 bat_temperature("bat_temperature",
                                                 ds_mxutils::OPTIONAL,
                                                 ds_mxutils::REQUIRED,
                                                 ds_mxutils::REQUIRED),
                                 bat_pressure("bat_pressure",
                                              ds_mxutils::OPTIONAL,
                                              ds_mxutils::REQUIRED,
                                              ds_mxutils::REQUIRED),
                                 bat_volts("bat_volts",
                                           ds_mxutils::REQUIRED,
                                           ds_mxutils::REQUIRED,
                                           ds_mxutils::REQUIRED),
                                 bat_ah("bat_ah", ds_mxutils::REQUIRED, ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED),
                                 bat_percent("bat_percent",
                                             ds_mxutils::REQUIRED,
                                             ds_mxutils::OPTIONAL,
                                             ds_mxutils::REQUIRED),
                                 xr1_code("xr1_code", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED),
                                 xr1_shortman("xr1_shortman",
                                              ds_mxutils::REQUIRED,
                                              ds_mxutils::OPTIONAL,
                                              ds_mxutils::REQUIRED),
                                 xr2_code("xr2_code", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED),
                                 xr2_shortman("xr2_shortman",
                                              ds_mxutils::REQUIRED,
                                              ds_mxutils::OPTIONAL,
                                              ds_mxutils::REQUIRED),
                                 depth_pressure("depth_pressure",
                                                ds_mxutils::OPTIONAL,
                                                ds_mxutils::REQUIRED,
                                                ds_mxutils::REQUIRED),
                                 depth_depth("depth_depth", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED)
{
  // abort message timeouts are disabled until an abort message is received
  abort_timeout = ros::TIME_MAX;
}

SentryMonitor::SentryMonitor(int argc, char *argv[], const std::string &name)
    : ds_base::DsProcess(argc, argv, name),
      main_humidity("main_humidity", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED),
      main_temperature("main_temperature", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED),
      main_pressure("main_pressure", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED),
      bat_humidity("bat_humidity", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED),
      bat_temperature("bat_temperature", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED),
      bat_pressure("bat_pressure", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED),
      bat_volts("bat_volts", ds_mxutils::REQUIRED, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED),
      bat_ah("bat_ah", ds_mxutils::REQUIRED, ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED),
      bat_percent("bat_percent", ds_mxutils::REQUIRED, ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED),
      xr1_code("xr1_code", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED),
      xr1_shortman("xr1_shortman", ds_mxutils::REQUIRED, ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED),
      xr2_code("xr2_code", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED),
      xr2_shortman("xr2_shortman", ds_mxutils::REQUIRED, ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED),
      depth_pressure("depth_pressure", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED),
      depth_depth("depth_depth", ds_mxutils::OPTIONAL, ds_mxutils::REQUIRED, ds_mxutils::REQUIRED)
{
  // abort message timeouts are disabled until an abort message is received
  abort_timeout = ros::TIME_MAX;
}

SentryMonitor::~SentryMonitor() = default;

void SentryMonitor::setupParameters() {
  ds_base::DsProcess::setupParameters();
  ros::NodeHandle nh = nodeHandle();
  std::string ns = ros::this_node::getName() + "/vars";

  main_humidity.loadParameters(nh, ns);
  main_temperature.loadParameters(nh, ns);
  main_pressure.loadParameters(nh, ns);
  bat_humidity.loadParameters(nh, ns);
  bat_temperature.loadParameters(nh, ns);
  bat_pressure.loadParameters(nh, ns);
  bat_volts.loadParameters(nh, ns);
  bat_ah.loadParameters(nh, ns);
  bat_percent.loadParameters(nh, ns);
  xr1_code.loadParameters(nh, ns);
  xr1_shortman.loadParameters(nh, ns);
  xr2_code.loadParameters(nh, ns);
  xr2_shortman.loadParameters(nh, ns);
  depth_pressure.loadParameters(nh, ns);
  depth_depth.loadParameters(nh, ns);

}

void SentryMonitor::setupSubscriptions() {
  ds_base::DsProcess::setupSubscriptions();

  // connect to our various subscriptions
  ros::NodeHandle nh;
  sub_abort = nh.subscribe("abort", 10, &SentryMonitor::abort_callback, this);
  sub_main_htp = nh.subscribe("main_htp", 10, &SentryMonitor::main_htp_callback, this);
  sub_bat_htp = nh.subscribe("bat_htp", 10, &SentryMonitor::bat_htp_callback, this);
  sub_bat_man = nh.subscribe("bat_man", 10, &SentryMonitor::bat_man_callback, this);
  sub_xr1 = nh.subscribe("xr1", 10, &SentryMonitor::xr1_callback, this);
  sub_xr2 = nh.subscribe("xr2", 10, &SentryMonitor::xr2_callback, this);
  sub_depth = nh.subscribe("depth", 10, &SentryMonitor::depth_callback, this);

}

void SentryMonitor::setupPublishers() {
  ds_base::DsProcess::setupPublishers();

  // setup our publisher for the eventlog topic
  ros::NodeHandle nh;
  pub_event = nh.advertise<ds_mx_msgs::MxEvent>("mx_event", 10, false);
}

void SentryMonitor::setupTimers() {
  ds_base::DsProcess::setupTimers();

  // setup a regular time to check timeouts
  ros::NodeHandle nh;
  timer_timeout_check = nh.createTimer(ros::Duration(10, 0), &SentryMonitor::timer_callback, this);
}

void SentryMonitor::abort_callback(const ds_core_msgs::Abort &msg) {
  if (msg.abort) {
    ROS_FATAL_STREAM("AbortManager requested an abort; passing error on to mission");
    send_fault("/fault/abort/abortmanager");
  }
  abort_timeout = msg.stamp + ros::Duration(msg.ttl);
}

void SentryMonitor::main_htp_callback(const ds_hotel_msgs::HTP& msg) {
  if (!main_humidity.checkValue(msg.humidity, msg.header.stamp)) {
    ROS_FATAL_STREAM("Main housing humidity fault detected " <<msg.humidity <<", signaling error");
    send_fault("/fault/leak/main/humidity");
  }
  if (!main_temperature.checkValue(msg.temperature, msg.header.stamp)) {
    ROS_FATAL_STREAM("Main housing temperature fault detected " <<msg.temperature <<", signaling error");
    send_fault("/fault/temp/main");
  }
  if (!main_pressure.checkValue(msg.pressure, msg.header.stamp)) {
    ROS_FATAL_STREAM("Main housing pressure fault detected " <<msg.pressure <<", signaling error");
    send_fault("/fault/leak/main/pressure");
  }
}

void SentryMonitor::bat_htp_callback(const ds_hotel_msgs::HTP& msg) {
  if (!bat_humidity.checkValue(msg.humidity, msg.header.stamp)) {
    ROS_FATAL_STREAM("Battery housing humidity fault detected " <<msg.humidity <<", signaling error");
    send_fault("/fault/leak/bat/humidity");
  }
  if (!bat_temperature.checkValue(msg.temperature, msg.header.stamp)) {
    ROS_FATAL_STREAM("Battery housing temperature fault detected " <<msg.temperature <<", signaling error");
    send_fault("/fault/temp/bat");
  }
  if (!bat_pressure.checkValue(msg.pressure, msg.header.stamp)) {
    ROS_FATAL_STREAM("Battery housing pressure fault detected " <<msg.pressure <<", signaling error");
    send_fault("/fault/leak/bat/pressure");
  }
}

void SentryMonitor::bat_man_callback(const ds_hotel_msgs::BatMan& msg) {
  if (!bat_ah.checkValue(msg.chargeAh, msg.header.stamp)) {
    ROS_FATAL_STREAM("Battery amp hours available (" <<msg.chargeAh <<") below minimum, signaling error");
    send_fault("/fault/battery/critical/ah");
  }
  if (!(bat_volts.checkValue(msg.maxModuleVolt, msg.header.stamp)
      && bat_volts.checkValue(msg.minModuleVolt, msg.header.stamp) )) {
    ROS_FATAL_STREAM("Battery module voltage out of range (" <<msg.minModuleVolt <<" -> " <<msg.maxModuleVolt
                                                             <<"), signaling error");
    send_fault("/fault/battery/critical/voltage");
  }
  if (!bat_percent.checkValue(msg.percentFull, msg.header.stamp)) {
    ROS_FATAL_STREAM("Battery percent charge (" <<msg.percentFull <<") below minimum, signaling error");
    send_fault("/fault/battery/critical/percent");
  }
}

void SentryMonitor::xr1_callback(const ds_hotel_msgs::XR& msg) {
  if (!xr1_shortman.checkValue(msg.short_deadsecs, msg.header.stamp)) {
    ROS_FATAL_STREAM("XR1 shortman expired; triggering abort");
    send_fault("/fault/abort/shortman/xr1");
  }
  if (!xr1_code.checkValue(msg.C, msg.header.stamp)) {
    ROS_FATAL_STREAM("XR1 has code " <<static_cast<int>(msg.C) <<"; triggering abort");
    send_fault("/fault/abort/acoustic/xr1");
  }
}

void SentryMonitor::xr2_callback(const ds_hotel_msgs::XR& msg) {
  if (!xr2_shortman.checkValue(msg.short_deadsecs, msg.header.stamp)) {
    ROS_FATAL_STREAM("XR2 shortman expired; triggering abort");
    send_fault("/fault/abort/shortman/xr2");
  }
  if (!xr2_code.checkValue(msg.C, msg.header.stamp)) {
    ROS_FATAL_STREAM("XR2 has code " <<static_cast<int>(msg.C) <<"; triggering abort");
    send_fault("/fault/abort/acoustic/xr2");
  }
}

void SentryMonitor::depth_callback(const ds_sensor_msgs::DepthPressure& msg) {
  if (!depth_pressure.checkValue(msg.pressure, msg.header.stamp)) {
    ROS_FATAL_STREAM("Pressure of " <<msg.pressure <<" dbar exceeds threshold.  Sending failure.");
    send_fault("/fault/environment/pressure");
  }
  if (!depth_depth.checkValue(msg.depth, msg.header.stamp)) {
    ROS_FATAL_STREAM("Depth of " <<msg.depth <<" meters exceeds threshold.  Sending failure.");
    send_fault("/fault/environment/depth");
  }
}

void SentryMonitor::timer_callback(const ros::TimerEvent& evt) {

  if (evt.current_real > abort_timeout) {
    ROS_FATAL_STREAM("No abort message within TTL.  Sending failure.");
    send_fault("/fault/abort/abortmanager/notresponding");
  }

  if (!(main_humidity.checkTimeoutOk(evt.current_real)
      && main_temperature.checkTimeoutOk(evt.current_real)
      && main_pressure.checkTimeoutOk(evt.current_real) )) {
    ROS_FATAL_STREAM("Main housing HTP sensor not reporting.  Sending failure.");
    send_fault("/fault/hotel/main_htp");
  }

  if (!(bat_humidity.checkTimeoutOk(evt.current_real)
      && bat_temperature.checkTimeoutOk(evt.current_real)
      && bat_pressure.checkTimeoutOk(evt.current_real) )) {
    ROS_FATAL_STREAM("Battery housing HTP sensor not reporting.  Sending failure.");
    send_fault("/fault/hotel/bat_htp");
  }

  if (!(bat_volts.checkTimeoutOk(evt.current_real)
      && bat_ah.checkTimeoutOk(evt.current_real)
      && bat_percent.checkTimeoutOk(evt.current_real) )) {
    ROS_FATAL_STREAM("Battery manager not reporting.  Sending failure.");
    send_fault("/fault/hotel/bat_man");
  }

  if (!(xr1_code.checkTimeoutOk(evt.current_real)
      && xr1_shortman.checkTimeoutOk(evt.current_real) )) {
    ROS_FATAL_STREAM("XR1 not reporting.  Sending failure.");
    send_fault("/fault/hotel/xr1");
  }

  if (!(xr2_code.checkTimeoutOk(evt.current_real)
      && xr2_shortman.checkTimeoutOk(evt.current_real) )) {
    ROS_FATAL_STREAM("XR2 not reporting.  Sending failure.");
    send_fault("/fault/hotel/xr2");
  }

  if (!(depth_pressure.checkTimeoutOk(evt.current_real)
      && !depth_depth.checkTimeoutOk(evt.current_real) )) {
    ROS_FATAL_STREAM("Depth sensor not reporting.  Sending failure.");
    send_fault("/fault/sensor/depth");
  }
}

void SentryMonitor::send_fault(const std::string& code) {
  ds_mx_msgs::MxEvent msg;

  msg.header.stamp = ros::Time::now();
  msg.eventid = code;

  pub_event.publish(msg);
}

} // namespace sentry_monitor

