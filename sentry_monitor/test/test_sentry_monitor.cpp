/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/24/19.
//

#include <memory>
#include <ros/ros.h>
#include <gtest/gtest.h>

#include <sentry_monitor/sentry_monitor.h>
#include <ds_mxutils/ds_test_message_queue.h>
#include <ds_mx_msgs/MxEvent.h>

class SentryMonitorTest : public ::testing::Test {
 public:
  SentryMonitorTest() : event_msgs("mx_event") {
    node_handle.reset(new ros::NodeHandle());
  }

 protected:

  void SetUp() override {
    evt_msg = ds_mx_msgs::MxEvent();

    underTest.reset(new sentry_monitor::SentryMonitor());
    underTest->setup();
  }

  std::unique_ptr<sentry_monitor::SentryMonitor> underTest;
  ds_mx::TestMessageQueue<ds_mx_msgs::MxEvent> event_msgs;
  std::unique_ptr<ros::NodeHandle> node_handle;

  ds_mx_msgs::MxEvent evt_msg;
  ros::Time test_time;
};

TEST_F(SentryMonitorTest, AbortManagerAbort) {
  // first, send a good abort message
  ros::Publisher abort_pub = node_handle->advertise<ds_core_msgs::Abort>("abort", 10, false);
  ds_core_msgs::Abort msg;
  msg.ttl = 10;
  msg.stamp = ros::Time(1000,0);
  msg.abort = false;
  msg.enable = true;
  abort_pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.stamp += ros::Duration(5);
  msg.abort = true;
  abort_pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/abort/abortmanager", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, AbortManagerTimeout) {
  // first, send a good abort message
  ros::Publisher abort_pub = node_handle->advertise<ds_core_msgs::Abort>("abort", 10, false);
  ds_core_msgs::Abort msg;
  msg.ttl = 10;
  msg.stamp = ros::Time(1000,0);
  msg.abort = false;
  msg.enable = true;
  abort_pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  ros::TimerEvent evt;
  evt.last_real = msg.stamp;
  evt.last_expected = evt.last_real;
  evt.current_real = msg.stamp + ros::Duration(10.001);
  evt.current_expected = evt.current_real;

  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/abort/abortmanager/notresponding", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, MainHumidityFail) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::HTP>("main_htp", 10, false);
  ds_hotel_msgs::HTP msg;
  msg.header.stamp = ros::Time(1000, 0);
  msg.humidity = 300;
  msg.temperature = 55;
  msg.pressure = 25;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.humidity = 300.001;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/leak/main/humidity", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/main_htp", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, MainTempFail) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::HTP>("main_htp", 10, false);
  ds_hotel_msgs::HTP msg;
  msg.header.stamp = ros::Time(1000, 0);
  msg.humidity = 300;
  msg.temperature = 55;
  msg.pressure = 25;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.temperature = 55.001;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/temp/main", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/main_htp", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, MainPressureFail) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::HTP>("main_htp", 10, false);
  ds_hotel_msgs::HTP msg;
  msg.header.stamp = ros::Time(1000, 0);
  msg.humidity = 300;
  msg.temperature = 55;
  msg.pressure = 25;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.pressure = 25.001;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/leak/main/pressure", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/main_htp", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, BatHumidityFail) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::HTP>("bat_htp", 10, false);
  ds_hotel_msgs::HTP msg;
  msg.header.stamp = ros::Time(1000, 0);
  msg.humidity = 100;
  msg.temperature = 40;
  msg.pressure = 10;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.humidity = 100.001;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/leak/bat/humidity", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/bat_htp", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, BatTempFail) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::HTP>("bat_htp", 10, false);
  ds_hotel_msgs::HTP msg;
  msg.header.stamp = ros::Time(1000, 0);
  msg.humidity = 100;
  msg.temperature = 40;
  msg.pressure = 10;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.temperature = 40.001;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/temp/bat", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/bat_htp", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, BatPressureFail) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::HTP>("bat_htp", 10, false);
  ds_hotel_msgs::HTP msg;
  msg.header.stamp = ros::Time(1000, 0);
  msg.humidity = 100;
  msg.temperature = 40;
  msg.pressure = 10;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.pressure = 10.001;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/leak/bat/pressure", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/bat_htp", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, BatManVoltsFail) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::BatMan>("bat_man", 10, false);
  ds_hotel_msgs::BatMan msg;
  msg.header.stamp = ros::Time(1000, 0);
  // note: the values are based on the limits in the file for testing, and are not
  // physically sensible
  msg.maxModuleVolt = 60;
  msg.minModuleVolt = 45;
  msg.chargeAh = 0;
  msg.percentFull = 6.0;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.maxModuleVolt = 60.001;
  msg.minModuleVolt = 45.0;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/battery/critical/voltage", evt_msg.eventid);

  msg.maxModuleVolt = 60.0;
  msg.minModuleVolt = 44.999;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/battery/critical/voltage", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/bat_man", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, BatManAhFail) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::BatMan>("bat_man", 10, false);
  ds_hotel_msgs::BatMan msg;
  msg.header.stamp = ros::Time(1000, 0);
  // note: the values are based on the limits in the file for testing, and are not
  // physically sensible
  msg.maxModuleVolt = 60;
  msg.minModuleVolt = 45;
  msg.chargeAh = 0;
  msg.percentFull = 6.0;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.chargeAh = -0.001;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/battery/critical/ah", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/bat_man", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, BatManPctFail) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::BatMan>("bat_man", 10, false);
  ds_hotel_msgs::BatMan msg;
  msg.header.stamp = ros::Time(1000, 0);
  // note: the values are based on the limits in the file for testing, and are not
  // physically sensible
  msg.maxModuleVolt = 60;
  msg.minModuleVolt = 45;
  msg.chargeAh = 0;
  msg.percentFull = 6.0;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.percentFull = 5.999;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/battery/critical/percent", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/bat_man", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, XR1Shortman) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::XR>("xr1", 10, false);
  ds_hotel_msgs::XR msg;
  msg.header.stamp = ros::Time(1000, 0);
  // note: the values are based on the limits in the file for testing, and are not
  // physically sensible
  msg.short_deadsecs = 1;
  msg.C = 14;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.short_deadsecs = 0;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/abort/shortman/xr1", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/xr1", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, XR1Code) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::XR>("xr1", 10, false);
  ds_hotel_msgs::XR msg;
  msg.header.stamp = ros::Time(1000, 0);
  // note: the values are based on the limits in the file for testing, and are not
  // physically sensible
  msg.short_deadsecs = 1;
  msg.C = 14;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.C = 15;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/abort/acoustic/xr1", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/xr1", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, XR2Shortman) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::XR>("xr2", 10, false);
  ds_hotel_msgs::XR msg;
  msg.header.stamp = ros::Time(1000, 0);
  // note: the values are based on the limits in the file for testing, and are not
  // physically sensible
  msg.short_deadsecs = 1;
  msg.C = 14;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.short_deadsecs = 0;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/abort/shortman/xr2", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/xr2", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, XR2Code) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_hotel_msgs::XR>("xr2", 10, false);
  ds_hotel_msgs::XR msg;
  msg.header.stamp = ros::Time(1000, 0);
  // note: the values are based on the limits in the file for testing, and are not
  // physically sensible
  msg.short_deadsecs = 1;
  msg.C = 14;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.C = 15;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/abort/acoustic/xr2", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/hotel/xr2", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, DepthPressureFail) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_sensor_msgs::DepthPressure>("depth", 10, false);
  ds_sensor_msgs::DepthPressure msg;
  msg.header.stamp = ros::Time(1000, 0);
  // note: the values are based on the limits in the file for testing, and are not
  // physically sensible
  msg.pressure = 6500;
  msg.depth = 6000;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.pressure = 6500.01;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/environment/pressure", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/sensor/depth", evt_msg.eventid);
}

TEST_F(SentryMonitorTest, DepthDepthFail) {
  // first, send a good abort message
  ros::Publisher pub = node_handle->advertise<ds_sensor_msgs::DepthPressure>("depth", 10, false);
  ds_sensor_msgs::DepthPressure msg;
  msg.header.stamp = ros::Time(1000, 0);
  // note: the values are based on the limits in the file for testing, and are not
  // physically sensible
  msg.pressure = 6500;
  msg.depth = 6000;
  pub.publish(msg);
  EXPECT_FALSE(event_msgs.waitForMessage(0.025)); // wait 25ms for another message; we shouldn't get any

  msg.depth = 6000.01;
  pub.publish(msg);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/environment/depth", evt_msg.eventid);

  ros::TimerEvent evt;
  evt.current_real = msg.header.stamp + ros::Duration(120.001);
  evt.current_expected = evt.current_real;
  underTest->timer_callback(evt);
  DSMX_EXPECT_MESSAGE(evt_msg, event_msgs, 0.150);
  EXPECT_EQ("/fault/sensor/depth", evt_msg.eventid);
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "rostest_sentry_monitor");
  ros::NodeHandle nh;

  ros::AsyncSpinner spinner(2); // use two threads
  spinner.start(); // returns immediately

  return RUN_ALL_TESTS();
}
