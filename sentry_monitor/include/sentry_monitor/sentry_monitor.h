/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/23/19.
//

#ifndef MX_SENTRY_MONITOR_H
#define MX_SENTRY_MONITOR_H

#include <string>

#include <ds_base/ds_process.h>
#include <ds_mxutils/var_monitor.h>

#include <ds_core_msgs/Abort.h>
#include <ds_hotel_msgs/HTP.h>
#include <ds_hotel_msgs/BatMan.h>
#include <ds_hotel_msgs/XR.h>
#include <ds_sensor_msgs/DepthPressure.h>

#include <ds_mx_msgs/MxEvent.h>

namespace sentry_monitor {

class SentryMonitor : public ds_base::DsProcess {
 public:
  explicit SentryMonitor();
  SentryMonitor(int argc, char* argv[], const std::string& name);
  ~SentryMonitor() override;

  DS_DISABLE_COPY(SentryMonitor);

  void setupParameters() override;
  void setupSubscriptions() override;
  void setupPublishers() override;
  void setupTimers() override;

 protected:
  // values to watch
  ds_mxutils::VarMonitor main_humidity;
  ds_mxutils::VarMonitor main_temperature;
  ds_mxutils::VarMonitor main_pressure;

  ds_mxutils::VarMonitor bat_humidity;
  ds_mxutils::VarMonitor bat_temperature;
  ds_mxutils::VarMonitor bat_pressure;

  ds_mxutils::VarMonitor bat_volts;
  ds_mxutils::VarMonitor bat_ah;
  ds_mxutils::VarMonitor bat_percent;

  ds_mxutils::VarMonitor xr1_code;
  ds_mxutils::VarMonitor xr1_shortman;

  ds_mxutils::VarMonitor xr2_code;
  ds_mxutils::VarMonitor xr2_shortman;

  ds_mxutils::VarMonitor depth_pressure;
  ds_mxutils::VarMonitor depth_depth;

  // ROS interface stuff
  ros::Publisher pub_event;

  ros::Subscriber sub_abort;
  ros::Subscriber sub_main_htp;
  ros::Subscriber sub_bat_htp;
  ros::Subscriber sub_bat_man;
  ros::Subscriber sub_xr1;
  ros::Subscriber sub_xr2;
  ros::Subscriber sub_depth;

  ros::Timer timer_timeout_check;

  // abort timeout check
  ros::Time abort_timeout;

  void abort_callback(const ds_core_msgs::Abort& msg);
  void main_htp_callback(const ds_hotel_msgs::HTP& msg);
  void bat_htp_callback(const ds_hotel_msgs::HTP& msg);
  void bat_man_callback(const ds_hotel_msgs::BatMan& msg);
  void xr1_callback(const ds_hotel_msgs::XR& msg);
  void xr2_callback(const ds_hotel_msgs::XR& msg);
  void depth_callback(const ds_sensor_msgs::DepthPressure& msg);

 public: // public, for testing
  void timer_callback(const ros::TimerEvent& evt);

 protected:
  void send_fault(const std::string& code);
};

} // namespace sentry_monitor

#endif //MX_SENTRY_MONITOR_H
