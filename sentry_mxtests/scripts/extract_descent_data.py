#! /usr/bin/env python

import sys
import os
import argparse
import scipy.io as sio
import glob
import datetime

from ds_sensor_msgs.msg import Ranges3D, Range3D
from ds_nav_msgs.msg import NavState

import rospy, rosbag

#divedir = "/data/archive/sentry-cruises/2018-kaiser/dives/sentry472"
#bagdir = os.path.join(divedir, "nav-sci", "raw", "rosbag")
#procdir = os.path.join(divedir, "nav-sci", "proc")
#outdir = '.'
#margin = 30*60

def extract_dive(bagdir, procdir, outdir, margin):
    diveconfig = sio.loadmat(os.path.join(procdir, 'dive_config.mat'))
    divename = diveconfig['dive_config'][0][0]['divename'][0]

    mission_times = sio.loadmat(os.path.join(procdir, '%s_mission_times.mat'%divename))
    launch_t = mission_times['mission_times']['launch_t'][0][0][0][0]
    survey_start_t = mission_times['mission_times']['survey_start_t'][0][0][0][0]

    record_start_time =  datetime.datetime.utcfromtimestamp(launch_t - margin)
    record_end_time =  datetime.datetime.utcfromtimestamp(survey_start_t + margin)
    duration = record_end_time - record_start_time

    print 'Loading data from directory \'%s\'' % bagdir
    print '    Dive %s' % divename
    print '    %s --> %s (%f hours)' % (record_start_time.isoformat(),
                                        record_end_time.isoformat(),
                                        duration.total_seconds()/3600)

    rosstart = rospy.Time.from_sec(launch_t - margin)
    rosend = rospy.Time.from_sec(survey_start_t + margin)

    with rosbag.Bag(os.path.join(outdir, 'descent_%s_sensors.bag' % divename), 'w') as bagout:
        bags = sorted(glob.glob(os.path.join(bagdir, 'sensors*.bag')))
        for bagname in bags:
            print 'Loading bag ', bagname
            bag = rosbag.Bag(bagname, 'r')
            for topic, msg, t in bag.read_messages(topics='/sentry/sensors/ranges'):
                if rosstart <= t and t <= rosend:
                    new_msg = Ranges3D()
                    new_msg.header = msg.header
                    new_msg.ranges = [None]*len(msg.ranges)
                    for i in range(0, len(msg.ranges)):
                        new_msg.ranges[i] = Range3D()
                        new_msg.ranges[i].range = msg.ranges[i].range
                        new_msg.ranges[i].range_validity = msg.ranges[i].range_validity
                        #if msg.ranges[i].range_validity != 0:
                        #    new_msg.ranges[i].range_quality = 250
                    bagout.write(topic, new_msg, t)

    with rosbag.Bag(os.path.join(outdir, 'descent_%s_nav.bag' % divename), 'w') as bagout:
        bags = sorted(glob.glob(os.path.join(bagdir, 'nav*.bag')))
        for bagname in bags:
            print 'Loading bag ', bagname
            bag = rosbag.Bag(bagname, 'r')
            for topic, msg, t in bag.read_messages(topics='/sentry/nav/agg/state'):
                if rosstart <= t and t <= rosend:
                    new_msg = NavState()
                    new_msg.header = msg.header
                    new_msg.ds_header = msg.ds_header
                    new_msg.lat = 0
                    new_msg.lon = 0
                    new_msg.northing = msg.northing.value
                    new_msg.easting = msg.easting.value
                    new_msg.down = msg.down.value
                    new_msg.roll = msg.roll.value
                    new_msg.pitch = msg.pitch.value
                    new_msg.heading = msg.heading.value
                    new_msg.surge_u = msg.surge_u.value
                    new_msg.sway_v = msg.sway_v.value
                    new_msg.heave_w = msg.heave_w.value
                    new_msg.p = msg.p.value
                    new_msg.q = msg.q.value
                    new_msg.r = msg.r.value

                    bagout.write('/sentry/nav/agg/navstate', new_msg, t)


parser = argparse.ArgumentParser(description="Convert one or more dives for use testing the new descent process")
parser.add_argument('--bagdir', '-b', default=os.path.join('nav-sci', 'raw', 'rosbag'))
parser.add_argument('--procdir', '-p', default=os.path.join('nav-sci', 'proc'))
parser.add_argument('--outdir', '-o', default='.')
parser.add_argument('--margin', '-m', default=30*60)
parser.add_argument('divedir', nargs='+')
args = parser.parse_args()

for d in args.divedir:
    print 'Extracting dive:', d
    extract_dive(os.path.join(d, args.bagdir), os.path.join(d, args.procdir), args.outdir, args.margin)




