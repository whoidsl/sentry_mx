#! /usr/bin/env python

import sys
import os
import argparse
import scipy.io as sio
import glob
import datetime

from ds_sensor_msgs.msg import Ranges3D, Range3D
from ds_nav_msgs.msg import NavState

import rospy, rosbag

#divedir = "/data/archive/sentry-cruises/2018-kaiser/dives/sentry472"
#bagdir = os.path.join(divedir, "nav-sci", "raw", "rosbag")
#procdir = os.path.join(divedir, "nav-sci", "proc")
#outdir = '.'
#margin = 30*60

def extract_dive(bagdir, procdir, outdir, margin):
    diveconfig = sio.loadmat(os.path.join(procdir, 'dive_config.mat'))
    divename = diveconfig['dive_config'][0][0]['divename'][0]

    mission_times = sio.loadmat(os.path.join(procdir, '%s_mission_times.mat'%divename))
    launch_t = mission_times['mission_times']['launch_t'][0][0][0][0]
    survey_start_t = mission_times['mission_times']['survey_start_t'][0][0][0][0]

    record_start_time =  datetime.datetime.utcfromtimestamp(launch_t - margin)
    record_end_time =  datetime.datetime.utcfromtimestamp(survey_start_t + margin)
    duration = record_end_time - record_start_time

    print 'Loading data from directory \'%s\'' % bagdir
    print '    Dive %s' % divename
    print '    %s --> %s (%f hours)' % (record_start_time.isoformat(),
                                        record_end_time.isoformat(),
                                        duration.total_seconds()/3600)

    rosstart = rospy.Time.from_sec(launch_t - margin)
    rosend = rospy.Time.from_sec(survey_start_t + margin)

    with open(os.path.join(outdir, 'descent_%s_dvl.csv' % divename), 'w') as outfd:
        bags = sorted(glob.glob(os.path.join(bagdir, 'sensors*.bag')))
        for bagname in bags:
            print 'Loading bag ', bagname
            bag = rosbag.Bag(bagname, 'r')
            for topic, msg, t in bag.read_messages(topics='/sentry/sensors/dvl300/pd0'):
                if rosstart <= t and t <= rosend:
                    outfd.write('%.6f %.4f %.4f %.4f %.4f %d %d %d %d %d %d %d %d\n' % (msg.header.stamp.to_sec(),
                                           msg.range[0], msg.range[1], msg.range[2], msg.range[3],
                                           msg.correlation[0], msg.correlation[1], msg.correlation[2], msg.correlation[3],
                                           msg.rssi_amp[0], msg.rssi_amp[1], msg.rssi_amp[2], msg.rssi_amp[3],
                                           ))


parser = argparse.ArgumentParser(description="Convert one or more dives for use testing the new descent process")
parser.add_argument('--bagdir', '-b', default=os.path.join('nav-sci', 'raw', 'rosbag'))
parser.add_argument('--procdir', '-p', default=os.path.join('nav-sci', 'proc'))
parser.add_argument('--outdir', '-o', default='.')
parser.add_argument('--margin', '-m', default=30*60)
parser.add_argument('divedir', nargs='+')
args = parser.parse_args()

for d in args.divedir:
    print 'Extracting dive:', d
    extract_dive(os.path.join(d, args.bagdir), os.path.join(d, args.procdir), args.outdir, args.margin)




