#! /usr/bin/env python

import rospy
import datetime
import sys

from ds_actuator_msgs.msg import DropweightCmd
from ds_sensor_msgs.msg import Ranges3D, Range3D
from ds_nav_msgs.msg import NavState
from sentry_msgs.msg import SentryMxGoal

class ResultEvaluator(object):
    def __init__(self,  divenum, divename):
        self._last_nav = None
        self._last_ranges = None

        self._divenum = divenum
        self._divename = divename

        self._range_sub = rospy.Subscriber('/sentry/sensors/ranges', Ranges3D, self.handle_ranges)
        self._nav_sub = rospy.Subscriber('/sentry/nav/agg/navstate', NavState, self.handle_nav)
        self._cmd_sub = rospy.Subscriber('/sentry/mx/sentry_cmd', SentryMxGoal, self.handle_cmd)
        self._weight_sub = rospy.Subscriber('/sentry/mx/sentry_dropweight_cmd', DropweightCmd, self.handle_drop)

        print 'Evaluating results for dive \"%s\"' % self._divename

    def handle_nav(self, msg):
        self._last_nav = msg

    def handle_ranges(self, msg):
        self._last_ranges = msg

    def handle_cmd(self, msg):
        if msg.controller_mode == 3:
            print 'Entering BALLAST CONTROL! dep=%.3f, alt=%.3f %.3f %.3f %.3f %.3f' % self.getState()
            filename = "/data/sentry_mx/results/result_%s_%s.log" % (datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S"), self._divename)
            print 'Writing logfile ', filename
            toWrite = ('%d, %.3f, %.3f,  %.3f,  %.3f,  %.3f,  %.3f' % ((self._divenum,) + self.getState()))
            print toWrite
            with open(filename, 'w') as outfd:
                outfd.write(toWrite + '\n')
            print 'Exiting...'
            rospy.signal_shutdown('Done!')

        print 'Got Command'

    def handle_drop(self, msg):
        print 'Dropping weights! dep=%.3f, alt=%.3f %.3f %.3f %.3f %.3f' % self.getState()

    def getState(self):
        dep = 0
        r1 = 1
        r2 = 1
        r3 = 1
        r4 = 1

        if self._last_nav:
            dep = self._last_nav.down

        if self._last_ranges:
            if self._last_ranges.ranges[0].range_validity != 0:
                r1 = self._last_ranges.ranges[0].range.point.z
            if self._last_ranges.ranges[1].range_validity != 0:
                r2 = self._last_ranges.ranges[1].range.point.z
            if self._last_ranges.ranges[2].range_validity != 0:
                r3 = self._last_ranges.ranges[2].range.point.z
            if self._last_ranges.ranges[3].range_validity != 0:
                r4 = self._last_ranges.ranges[3].range.point.z

        return dep, r1, r2, r3, r4, self._last_nav.header.stamp.to_sec()

def main():
    divenum = rospy.get_param('/divenum')
    divename = rospy.get_param('/divename')
    rospy.init_node('descent_evaluator')
    eval = ResultEvaluator(divenum, divename)
    rospy.spin()
